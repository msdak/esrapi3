import com.somansa.privacyi.api.PIScanLib;


public class APITest {

	public static String TestScan(String[] args){
		if ("".equals(args[0]) || "".equals(args[1])) {
			return "usage: PIScanLib [data_path] [target_file_path]";
		}
		
		try {			
			PIScanLib.init(args[0]);
			String s = PIScanLib.ScanPrivacyFile(args[1]);
			return s;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static void main(String[] args) {
		System.out.println(TestScan(args)); 
	}

}
