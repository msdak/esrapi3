package com.solupia.somansa.config;

/**
 * Created by MeongSoo.Jang on 2017. 2. 7..
 *
 * DLP에서 도출 되는 개인정보 코드값값
 *
 * 주민번호
 * 여권번호
 * 통합계좌번호
 * 신용카드
 * 신한금융투자 계좌번호
 * 운전면허
 *
 */
public enum MinimumQuantityCode {

    RES_REG_CODE("ResRegCode"),
    TRANSFER_ACCOUNT("TransferAccount"),
    CREDIT_CARD("CreditCard"),
    CELL_PHONE("CellPhone"),
    PHONE("Phone"),
    E_MAIL("Email"),
    DETECT_IMPOSSIBILITY("DetectImpossibility");

    private String memberName;

    MinimumQuantityCode(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberName() {
        return this.memberName;
    }

    public static MinimumQuantityCode getQuantityCode(String source) {
        if (source.contains(SomansaDLPConfig.RES_REG_CODE_PREFIX)) {
            return MinimumQuantityCode.RES_REG_CODE;
        }else if(source.contains(SomansaDLPConfig.CREDIT_CARD_PREFIX)) {
            return MinimumQuantityCode.CREDIT_CARD;
        }else if(source.contains(SomansaDLPConfig.TRANSFER_ACCOUNT_PREFIX)) {
            return MinimumQuantityCode.TRANSFER_ACCOUNT;
        }else if(source.contains(SomansaDLPConfig.PHONE_REFIX )) {
            return MinimumQuantityCode.PHONE;
        }else if(source.contains(SomansaDLPConfig.CELL_PHONE_REFIX)) {
            return MinimumQuantityCode.CELL_PHONE;
        }else if(source.contains(SomansaDLPConfig.E_MAIL_PREFIX)) {
            return MinimumQuantityCode.E_MAIL;
        }else  {
            return MinimumQuantityCode.DETECT_IMPOSSIBILITY;
        }
    }
}
