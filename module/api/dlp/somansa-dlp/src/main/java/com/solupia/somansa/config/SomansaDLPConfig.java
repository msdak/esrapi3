package com.solupia.somansa.config;

/**
 * Created by solupia on 2017. 2. 24..
 */
public class SomansaDLPConfig {

    public static String SOMANSA_PATH = "D:\\serveriAPI\\serveri\\ProgramData\\";
    public static String SOMANSA_DIR_PAHT = "D:\\eSpider\\DLP\\";

    public static final String RES_REG_CODE_PREFIX = "주민 등록 번호";
    public static final String TRANSFER_ACCOUNT_PREFIX = "계좌 번호";
    public static final String CREDIT_CARD_PREFIX = "신용 카드 번호";

    public static final String PHONE_REFIX = "전화 번호";
    public static final String CELL_PHONE_REFIX = "핸드폰 번호";
    public static final String E_MAIL_PREFIX = "E-Mail 주소";

    //개인 정보 검출 불가 파일
    public static final String DETECT_IMPOSSIBILITY = "UnsupportedDataType";

}
