package com.solupia.somansa.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by solupia on 2017. 2. 24..
 */
@XmlRootElement(name = "Pattern")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter
@Setter
public class Pattern {

    @XmlAttribute(name = "FileName")
    private String fileName;

    @XmlAttribute(name = "FilePath")
    private String filePath;

    @XmlAttribute(name = "PatternName")
    private String patternName;

    @XmlAttribute(name = "PatternGUID")
    private String patternGUID;

    @XmlAttribute(name = "Repetition")
    private Long repetition;
}
