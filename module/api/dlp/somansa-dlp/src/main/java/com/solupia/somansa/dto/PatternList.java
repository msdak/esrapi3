package com.solupia.somansa.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by solupia on 2017. 2. 24..
 */
@XmlRootElement(name = "PatternList")
@XmlAccessorType(XmlAccessType.FIELD)
@Getter @Setter
public class PatternList {

    @XmlElement(name = "Pattern")
    List<Pattern> PatternList;

    @XmlAttribute(name = "Exception")
    private String exception;
}
