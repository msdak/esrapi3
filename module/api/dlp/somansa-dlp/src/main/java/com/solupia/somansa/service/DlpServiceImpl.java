package com.solupia.somansa.service;

import com.solupia.dlp.comm.dto.DetectType;
import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.dto.MinimumQuantityData;
import com.solupia.dlp.comm.service.DlpService;
import com.solupia.somansa.config.MinimumQuantityCode;
import com.solupia.somansa.config.SomansaDLPConfig;
import com.solupia.somansa.config.SomansaMinimumQuantityData;
import com.solupia.somansa.dto.Pattern;
import com.solupia.somansa.dto.PatternList;
import com.somansa.privacyi.api.PIScanLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by solupia on 2017. 2. 24..
 */
public class DlpServiceImpl implements DlpService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DlpServiceImpl.class);

    static  {
        try{
            PIScanLib.init(SomansaDLPConfig.SOMANSA_PATH);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public PatternList parsingXML(String source) {
        PatternList patternList = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PatternList.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            patternList = (PatternList)unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("DLP XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return patternList;
    }

    @Override
    public FileInfo parsingContent(FileInfo fileInfo, MinimumQuantityData minimumQuantityData, String charSet) {
        String fileFullPath = SomansaDLPConfig.SOMANSA_DIR_PAHT+ fileInfo.getDirPath() + File.separator + fileInfo.getFileName();
        minimumQuantityData.setDetectImpossibility(1L);
        LOGGER.info("fileFullPath is : {}" , fileFullPath);
        DetectType detectType = DetectType.NON_DETECTABLE;
        PatternList patternList = parsingXML(PIScanLib.ScanPrivacyFile(fileFullPath));

        if(patternList.getException() != null) {
            //검출불가
            detectType = DetectType.NON_DETECTABLE;
            fileInfo.setNonDetectReason(patternList.getException());
        }else if(patternList.getPatternList() == null){
            detectType = DetectType.NOT_DETECTABLE;
        }else if(patternList.getPatternList() != null ){
            detectType = DetectType.NOT_DETECTABLE;
            SomansaMinimumQuantityData fileInfoQuntiData = new SomansaMinimumQuantityData();
            for(Pattern pattern : patternList.getPatternList()) {
                //분석실패
                if(pattern.getPatternName().equals("TextFilteringFailure") || pattern.getPatternName().equals("UnauthorizedEncryption")
                        || pattern.getPatternName().equals("UnsupportedDataType")){
                    detectType = DetectType.NON_DETECTABLE;
                    fileInfo.setNonDetectReason(pattern.getPatternName()); //분석실패사유
                //분석성공
                } else {
                    MinimumQuantityCode quantityCode = MinimumQuantityCode.getQuantityCode(pattern.getPatternName());
                    Long currentQuantity = pattern.getRepetition();
                    Long minimumQuantity = this.getMinimumQuantityData(minimumQuantityData, quantityCode);
                    detectType = this.isPrivacyInfo(currentQuantity, minimumQuantity, detectType);
                    this.setQuantityData(fileInfoQuntiData, quantityCode, currentQuantity);
                }
            }
            fileInfoQuntiData.setQuntityDataResult();
            //검출 불가 정보가 있으면 검출 불가 상태로 변경
            if(fileInfoQuntiData.getDetectImpossibility() > 0) detectType = DetectType.NON_DETECTABLE;
            fileInfo.setQuantityData(fileInfoQuntiData);
        }
        fileInfo.setPrivacyInfo(detectType);
        return fileInfo;
    }

    @Override
    public boolean isDlpCompl(List<FileInfo> fileList) {
        int cnt = 0;
        for(FileInfo fileInfo: fileList) {
            String s = SomansaDLPConfig.SOMANSA_DIR_PAHT + fileInfo.getDirPath() + File.separator + fileInfo.getFileName();
            LOGGER.info("isDLPCompl : {}", s);
            File file1 = new File(s);
            if(file1.exists()) {
                cnt ++;
            }
        }
        return fileList.size() == cnt;
    }

    @Override
    public String getDefaultDLPPath() {
        return SomansaDLPConfig.SOMANSA_DIR_PAHT;
    }

    /**
     *
     * 임계치 값을 불러오는 메소드
     *
     * @param minimumQuantityData
     * @param quantityCode
     * @return
     */
    private  Long getMinimumQuantityData(MinimumQuantityData minimumQuantityData, MinimumQuantityCode quantityCode) {
        Long minimumQuantity = 0L;
        try {
            minimumQuantity =
                    (Long) minimumQuantityData.getClass()
                            .getMethod("get" + quantityCode.getMemberName())
                            .invoke(minimumQuantityData);
        } catch (NoSuchMethodException e) {
            LOGGER.error("parsingError", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("parsingError", e);
        } catch (InvocationTargetException e) {
            LOGGER.error("parsingError", e);
        }
        return minimumQuantity;
    }

    /**
     * 임계치 이상이면 개인정보 검출대상 파일
     *
     * @param currentQuantity
     * @param minimumQuantity
     * @param privacyInfo
     * @return
     */
    private DetectType isPrivacyInfo(Long currentQuantity, Long minimumQuantity, DetectType privacyInfo) {
        System.out.println("isPrivacyinfo.. : " + currentQuantity + " , mini : " + minimumQuantity);
        DetectType detectType;
        if(privacyInfo == null) {
            detectType = DetectType.NOT_DETECTABLE;
        }else {
            detectType = privacyInfo;
        }
        if(minimumQuantity != -1L && currentQuantity >= minimumQuantity) {
            detectType = DetectType.BE_DETECTED;
        }
        return detectType;
    }

    private  void setQuantityData(SomansaMinimumQuantityData minimumQuantityData, MinimumQuantityCode quantityCode, Long data) {
        try {
            minimumQuantityData.getClass()
                    .getMethod("set" + quantityCode.getMemberName(), Long.class)
                    .invoke(minimumQuantityData, data);
        } catch (NoSuchMethodException e) {
            LOGGER.error("parsingError", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("parsingError", e);
        } catch (InvocationTargetException e) {
            LOGGER.error("parsingError", e);
        }
    }
}

