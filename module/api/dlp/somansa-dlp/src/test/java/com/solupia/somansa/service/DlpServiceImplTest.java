package com.solupia.somansa.service;

import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.dto.MinimumQuantityData;
import com.solupia.somansa.dto.PatternList;

import java.util.Arrays;
import java.util.List;

/**
 * Created by solupia on 2017. 2. 24..
 */
public class DlpServiceImplTest {
/*

    @org.junit.Test
    public void parsingXML() throws Exception {

        String source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
                "04727 243-]]></Pattern></PatternList>";

        source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='TextFilteringFailure.ppt' FilePath='C:\\SERVERI\\privacyfilter\\serveri\\sample_data\\Samples'\n" +
                "PatternName='TextFilteringFailure' PatternGUID='8e18a19c-1c98-49cc-8acd-211147c372c2' Repetition='1' AwarenessType='1' ><![CDATA[]]></Pattern></PatternList>";
        DlpServiceImpl dlpService = new DlpServiceImpl();

        PatternList patternList = dlpService.parsingXML(source);

        System.out.println("1"+patternList.toString());
        System.out.println("2"+patternList.getException() == null);
        source = "<?xml version='1.0' encoding='UTF-8'?><PatternList Exception=\"Not Exist-[D:\\serveriAPI\\serveri\\sample_databinary.doc]\" />";

        patternList = dlpService.parsingXML(source);
        System.out.println("3"+patternList.getException());

        source = "<?xml version='1.0' encoding='UTF-8'?><PatternList></PatternList>";

        patternList = dlpService.parsingXML(source);
        System.out.println("4"+patternList.getException());

        source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
                "04727 243-]]></Pattern><Pattern FileName='doc_test.docx' PatternName='UnsupportedDataType' Repetition='1'></Pattern></PatternList>";

        patternList = dlpService.parsingXML(source);
        System.out.println("5"+patternList.getException());


    }

    @org.junit.Test
    public void parsingContent() throws Exception {
//        String source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
//                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
//                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
//                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
//                "04727 243-]]></Pattern></PatternList>";
//
//        DlpServiceImpl dlpService = new DlpServiceImpl();
//        dlpService.parsingContent(new FileInfo(), new MinimumQuantityData(), "MS949");

        MinimumQuantityData minimumQuantityData = new MinimumQuantityData();
        minimumQuantityData.setTransferAccount(20000L);
        minimumQuantityData.setResRegCode(10L);

        String source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
                "04727 243-]]></Pattern><Pattern FileName='doc_test.docx' PatternName='UnsupportedDataType' Repetition='1'></Pattern></PatternList>";

//        String source = "<?xml version='1.0' encoding='UTF-8'?>" +
//                "<PatternList>" +
//                "<Pattern FileName='5edi7xuzQIci2S108dJeZVCBr40IutKv.pdf' " +
//                "FilePath='D:\\eSpider\\DLP\\qDCpXt0ikCvkIoWIij2gbs0mxWTUvIGV' PatternName='¿¸»≠ π¯»£' PatternGUID='2dc3c45a-ac81-4115-b7fd-c7d7b305e723' Repetition='1' >" +
//                "<![CDATA[02-3480-4561 ]]></Pattern><Pattern FileName='5edi7xuzQIci2S108dJeZVCBr40IutKv.pdf' FilePath='D:\\eSpider\\DLP\\qDCpXt0ikCvkIoWIij2gbs0mxWTUvIGV' " +
//                "PatternName='«⁄µÂ∆˘ π¯»£' PatternGUID='84c98d92-683b-499c-9bae-b1d6e89153d6' Repetition='1' ><![CDATA[010-2033-0210 ]]></Pattern>" +
//                "<Pattern FileName='5edi7xuzQIci2S108dJeZVCBr40IutKv.pdf' FilePath='D:\\eSpider\\DLP\\qDCpXt0ikCvkIoWIij2gbs0mxWTUvIGV' PatternName='E-Mail ¡÷º“' " +
//                "PatternGUID='ddcaa41f-03f5-461b-945f-01f913d8d1af' Repetition='1' ><![CDATA[jlegend1@naver.com ]]></Pattern></PatternList>";

//        String source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
//                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
//                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
//                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
//                "04727 243-]]></Pattern>" +
//                "<Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
//                "ple_data' PatternName='주민 등록 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
//                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
//                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
//                "04727 243-]]></Pattern>"+
//                "</PatternList>";
        DlpServiceImpl dlpService = new DlpServiceImpl();

        FileInfo ms949 = dlpService.parsingContent(new FileInfo(), minimumQuantityData, "MS949");
        System.out.println(ms949.getPrivacyInfo());
    }

    @org.junit.Test
    public void isDlpCompl() throws Exception {

        List<String> strings = Arrays.asList("123", "123123");

        boolean b = strings.stream().anyMatch(e -> e.length() > 3);

        System.out.println(strings.get(0));

        System.out.println(b);
    }
*/

}