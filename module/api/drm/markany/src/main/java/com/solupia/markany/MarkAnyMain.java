package com.solupia.markany;

import com.markany.nt.WDSCryptAll;
import com.solupia.markany.dec.MarkAnyDecUtils;

/**
 * Created by solupia on 2017. 2. 16..
 */
public class MarkAnyMain {

    public static void main(String[] args) {
        if(args.length == 0){
            System.out.println("=============== Input Sample ================");
            System.out.println("복호화 테스트 : java JniTest dec");
            System.out.println("암호화 테스트 : java JniTest enc");
            System.out.println("암복호화 테스트 : java JniTest encdec");
            System.out.println();
        }
        else if(args.length>0 && "chk".equals(args[0]))
        {
            /**
             [파일체크]
             파일이 암호화 되어 있는지 확인함
             */
            // 생성자 초기화
            WDSCryptAll m_enc_dec = new WDSCryptAll();

            // 복호화 대상 파일 Mapping
            //m_enc_dec.sSourceFilePath = "D:\\MarkAny_Jni\\Samples\\Enc\\admin_개인한_암호화.ppt";			// 2010 R2 암호화 파일
            m_enc_dec.sSourceFilePath = args[1];		// 일반 파일

            // 복호화 함수 호출
            int chk_rs = m_enc_dec.iEncCheck();
            System.out.println("파일 체크 결과 : "+chk_rs);
        }else if(args.length > 0  && "enc".equals(args[0])) {
            /**
             [파일체크]
             파일이 암호화 되어 있는지 확인함
             */
            // 복호화 함수 호출
            int chk_rs = MarkAnyDecUtils.toExtract(args[1]);
            System.out.println("파일 체크 결과 : "+chk_rs);
        }else if(args.length > 0  && "wat".equals(args[0])) {
            /**
             [파일체크]
             파일이 암호화 되어 있는지 확인함
             */
            // 복호화 함수 호출
//            int chk_rs = MarkAnyDecUtils.toWaterMark(args[1], args[2], args[3]);
//            System.out.println("파일 체크 결과 : "+chk_rs);
        }else if(args.length > 0  && "dec".equals(args[0])) {
            /**
             [파일체크]
             파일이 암호화 되어 있는지 확인함
             */
            // 복호화 함수 호출
            int chk_rs = MarkAnyDecUtils.toEncOpen(args[1], args[2], args[3], args[4]);
            System.out.println("파일 체크 결과 : "+chk_rs);
        }

    }
}
