package com.solupia.markany;

import com.markany.nt.*;
import com.solupia.markany.config.MarkAnyConfig;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Sample {

    public static void main(String[] args)
    {
        WDSCryptAll m_enc_dec = null;


        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

        if(args.length==0)
        {
            System.out.println("=============== Input Sample ================");
            System.out.println("복호화 테스트 : java JniTest dec");
            System.out.println("암호화 테스트 : java JniTest enc");
            System.out.println("암복호화 테스트 : java JniTest encdec");
            System.out.println();
        }
        else if(args.length>0 && "chk".equals(args[0]))
        {
            /**
             [파일체크]
             파일이 암호화 되어 있는지 확인함
             */
            // 생성자 초기화
            m_enc_dec = new WDSCryptAll();

            // 복호화 대상 파일 Mapping
            //m_enc_dec.sSourceFilePath = "D:\\MarkAny_Jni\\Samples\\Enc\\admin_개인한_암호화.ppt";			// 2010 R2 암호화 파일
            m_enc_dec.sSourceFilePath = args[1];		// 일반 파일

            // 복호화 함수 호출
            int chk_rs = m_enc_dec.iEncCheck();
            System.out.println("파일 체크 결과 : "+chk_rs);
        }
        else if(args.length>0 && "dec".equals(args[0]))
        {
            /**
             [복호화]
             srcFile에서 DestFile로 파일 복호화
             DestFile이 null인 경우 srcFile을 복호화 함
             */

            // 생성자 초기화
            m_enc_dec = new WDSCryptAll();

            // 복호화 대상 파일 Mapping
            m_enc_dec.sSourceFilePath = args[1];
            m_enc_dec.sDestFilePath = args[2];

            // 복호화 함수 호출
            int dec_rs = m_enc_dec.iDecrypt();
            System.out.println("파일 복호화 결과 : "+dec_rs);
        }
        else if(args.length>0 && "enc".equals(args[0]))
        {
            /**
             [암호화]
             srcFile에서 DestFile로 파일 암호화
             DestFile이 null인 경우 srcFile을 암호화 함
             */
            m_enc_dec = new WDSCryptAll();
//			m_enc_dec.sDrmServerIp = "127.0.0.1";		// DRM Encrypt Module 서버 IP?


            m_enc_dec.sSourceFilePath = args[1];		// [필수] 암호화 대상 파일
            m_enc_dec.sDestFilePath = args[2];			// [선택] 암호화 후 파일
            m_enc_dec.sUserId = "admin";								// [필수] 사용자 아이디 또는 사번
            m_enc_dec.sEnterpriseId = MarkAnyConfig.COMPANY_ID;		// [필수] 그룹아이디
            m_enc_dec.sEnterpriseName = "통합 승인 연동";					// [선택] 그룹명
            m_enc_dec.sCompanyId = MarkAnyConfig.COMPANY_ID;			// [필수] 회사아이디
            m_enc_dec.sCompanyName = "통합 승인연동";					// [선택] 회사명

            m_enc_dec.sDocId
                    = "admin_" + LocalDateTime.now().format(timeFormatter);
                                                                    // [필수] 파일별 Unique해야함 (로그 분석 시 사용되는 키값)
            m_enc_dec.sDocTitle = "DRM Doc Title";						// [선택] 문서 타이틀
            //m_enc_dec.sDocGrade = "대외비";								// [선택] 문서 등급명
            m_enc_dec.sServerOrigin = "PLM";							// [필수] 서버명 또는 시스템 약어
            m_enc_dec.sEncryptedBy = "0";								// [필수] 암호화 파일 속성 (0:서버 암호화, 1:자동 암호화, 2:암호화 툴 사용)
            m_enc_dec.sFileName = args[3];		// [필수] 파일명칭 (암호화 헤더에 들어가는 파일명)
            //m_enc_dec.sDocCreatorId = "admin";							// [선택] 파일 최초 생성자 아이디
            //m_enc_dec.sMachineKey = "";									// [선택] 서버 머신키

            // [ACL 설정]
            m_enc_dec.sDocExchangePolicy = "1";							// [필수] 문서 교환 정책 (0:개인한, 1:사내한, 2:부서한)
            //m_enc_dec.sDocOfflinePolicy = "2";						// [선택] 오프라인 정책 (1:암호화 문서 사용불가, 2:암호화 문서 읽기 전용)
            m_enc_dec.iDocOpenCount = -99;								// [필수] 오픈 횟수 (0~999 입력, -99 : 무제한)
            m_enc_dec.iDocPrintCount = -99;								// [필수] 인쇄 횟수 (0~999 입력, -99 : 무제한)
            m_enc_dec.iCanSave = 1;										// [필수] 저장 가능 여부 (0:저장 불가능, 1:저장 가능)
            m_enc_dec.iCanEdit = 1;										// [필수] 편집 가능 여부 (0:편집 불가능, 1:편집 가능)
            m_enc_dec.iClipboardOption = 0;								// [필수] 암호화 문서의 클립보드 제어(0:클립보드 제어, 1:제어하지 않음)
            m_enc_dec.iVisiblePrint = 1;								// [필수] 워터마크 적용 여부 (0:적용 안함, 1:적용함)
            m_enc_dec.iImageSafer = 0;									// [필수] 화면캡쳐 방지 여부 (1:방지, 0:허용 or 방지하지않음)
            m_enc_dec.sDocValidPeriod = "-99";							// [필수] sValidPeriodType 값이 1이면 숫자형식, 값이 2이면 yyyymmddhhmmss
            m_enc_dec.iDocOpenLog = 1;									// [필수] 오픈 시 로그 전송 (1:전송, 0:미전송)
            m_enc_dec.iDocSaveLog = 1;									// [필수] 저장 시 로그 전송 (1:전송, 0:미전송)
            m_enc_dec.iDocPrintLog = 1;									// [필수] 인쇄 시 로그 전송 (1:전송, 0:미전송)

            m_enc_dec.sServerInfo_Log = "drm.donghee.co.kr:40002";		// [필수] 로그 전송 url:port
            m_enc_dec.iOnlineAclControl = 0;							// [필수] 실시간 권한 제어(0:미사용, 1:사용)
            m_enc_dec.sDocType = args[4];									// [필수] 파일명 확장자
            //m_enc_dec.sGroupId = "0";									// [선택] 그룹코드
            //m_enc_dec.sGroupName = "그룹명";							// [선택] 그룹명
            //m_enc_dec.sPositionId = "0";								// [선택] 직급코드
            //m_enc_dec.sPositionName = "직급명";							// [선택] 직급명
            //m_enc_dec.sUserName = "사용자";								// [선택] 사용자이름 (sUserId 아이디를 가지는 사용자 이름)
            //m_enc_dec.sDeptID = "0";									// [선택/필수] 부서코드 (문서교환정책이 부서한인 경우 필수)
            //m_enc_dec.sDeptName = "부서명";								// [선택] 부서명
            //m_enc_dec.sPositionLevel = "";								// [선택] 직위(급) 등급
            m_enc_dec.sSecurityLevel = "0";								// [필수] 보안 등급 코드 (동희산업의 경우 0:암호화, 1:대외비)
            m_enc_dec.sSecurityLevelName = "암호화";					// [필수] 보안 등급명
            //m_enc_dec.sMultiUserID = "";								// [선택] 다중 사용자 계정 (문서교환정책이 멀티 유저인 경우 사용)
            //m_enc_dec.sMultiUserName = "";								// [선택] 다중 사용자 이름 (문서교환정책이 멀티 유저인 경우 사용)
            m_enc_dec.sCreatorName = "admin";							// [선택] 생성자 이름
            m_enc_dec.sUsableAlways = "0";								// [선택] 항상 사용 가능 설정 값 (1:무조건 열람)
            m_enc_dec.sValidPeriodType = "1";							// [필수] (1:sDocValidPeriod의 값을 숫자로 표기, 0:sDocValidPeriod의 값을 yyyymmddhhmmss로 표기)
            //m_enc_dec.sCreatorCompanyId = "DONGHEE-765D-18B2-6E3D";		// [선택] 생성자 소속 회사 아이디 (ID는 마크애니에서 발급)
            //m_enc_dec.sCreatorDeptId = "0";								// [선택] 생성자 소속 부서 ID
            //m_enc_dec.sCreatorGroupId = "0";							// [선택] 생성자 소속 그룹 ID
            //m_enc_dec.sCreatorPositionId = "0";							// [선택] 생성자 소속 직위(급) ID
            //m_enc_dec.sReserved01 = "";									// [선택] 공백
            //m_enc_dec.sReserved02 = "";									// [선택] 공백
            //m_enc_dec.sReserved03 = "";									// [선택] 공백
            //m_enc_dec.sReserved04 = "";									// [선택] 공백
            //m_enc_dec.sReserved05 = "";									// [선택] 공백
            m_enc_dec.iEncryptPrevCipher = 0;							// [선택] 0:신버전 암호화, 1:구버전 암호화, 디폴트 : 신버전 암호화

            int enc_rs = m_enc_dec.iEncrypt();
            System.out.println("파일 암호화 결과 : "+enc_rs);

        }



    }
}