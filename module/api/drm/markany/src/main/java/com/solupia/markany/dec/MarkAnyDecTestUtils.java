package com.solupia.markany.dec;

import com.solupia.markany.config.MarkAnyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Created by solupia on 2017. 2. 20..
 */
public class MarkAnyDecTestUtils {

    private static Logger LOGGER  = LoggerFactory.getLogger(MarkAnyDecTestUtils.class);
    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    private static int comononDec(String filePath) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;
            Files.copy(Paths.get(encFullPath), Paths.get(encFullPath));
            dec_rs = 0;
        }catch (Exception e) {
            dec_rs = 999999;
        }
        return dec_rs;
    }

    private static int comononDec(String filePath, String target) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;
            File file = new File(target);
            file.getParentFile().mkdir();
            Files.copy(Paths.get(encFullPath), Paths.get(target));
            dec_rs = 0;
        }catch (Exception e) {
            dec_rs = 999999;
        }
        return dec_rs;
    }

    private static int comononEnc(String filePath) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;

            List<String> strings = Files.readAllLines(Paths.get(encFullPath), Charset.forName("ISO_8859_1"));
            strings.add("<!DOCTYPE PSW>>");
            Path write = Files.write(Paths.get(encFullPath), strings);

            LOGGER.info("파일 암호화 결과 : {}", dec_rs);
            dec_rs = 0;
        }catch (Exception e) {
            dec_rs = 999999;
        }
        return dec_rs;
    }

    private static int comononEnc(String filePath, String target) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;
            File file = new File(target);
            file.getParentFile().mkdir();
            List<String> strings = Files.readAllLines(Paths.get(encFullPath), Charset.forName("ISO_8859_1"));
            strings.add("<!DOCTYPE PSW>>");
            Path write = Files.write(Paths.get(target), strings);
            LOGGER.info("파일 암호화 결과 : {}", dec_rs);
            dec_rs = 0;
        }catch (Exception e) {
            dec_rs = 999999;
        }
        return dec_rs;
    }


    /**
     * 파일 복호화
     * @param filePath
     * @return
     */
    public static int toExtract(String filePath, String target) {
        return comononDec(filePath, target);
    }

    /**
     * 파일 복호화 마크애니 설정 외 경로
     * @param filePath
     * @return
     */
    public static int extractToExternSource(String filePath, String fileFullPath) {
        return comononDec(filePath, fileFullPath);
    }


    /**
     * 파일 복호화
     * @param filePath
     * @return
     */
    public static int extractToSource(String filePath) {
//        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        return comononDec(filePath, MarkAnyConfig.ENCFILE_DIR + filePath);
    }

    /**
     * 워터마크 해제
     * @param filePath
     * @return
     */
    public static int toWaterMark(String filePath, String userId, String fileName, String ext) {
        String decFullPath = MarkAnyConfig.ENCFILE_DIR + filePath;
        comononDec(filePath);
        //복호화 경로
        return comononEnc(filePath, decFullPath);
    }

    /**
     *
     * 결재자 확인 시 열기
     *
     * 설정 값
     * iDocOpenCount  오픈 횟수 (0~999 입력, -99 : 무제한)
       iDocPrintCount 인쇄 횟수 (0~999 입력, -99 : 무제한)
       iCanSave  저장 가능 여부 (0:저장 불가능, 1:저장 가능)
       iCanEdit  편집 가능 여부 (0:편집 불가능, 1:편집 가능)
       iImageSafer  화면캡쳐 방지 여부 (1:방지, 0:허용 or 방지하지않음)
       sDocGrade  문서 등급명 EZS
     *
     * @param filePath
     * @return
     */
    public static int toEncOpen(String filePath, String userId, String fileName, String ext) {
        comononDec(filePath);
        return comononEnc(filePath);
    }
}
