package com.solupia.markany.dec;

import com.markany.nt.WDSCryptAll;
import com.solupia.markany.config.MarkAnyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 2. 20..
 */
public class MarkAnyDecUtils {

    private static Logger LOGGER  = LoggerFactory.getLogger(MarkAnyDecUtils.class);
    private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    private static int comononDec(String filePath, WDSCryptAll wdsCryptAll) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;
            LOGGER.info("ENC : {}" , encFullPath);
            wdsCryptAll.sSourceFilePath = encFullPath;		// 일반 파일
            // 복호화 함수 호출
            wdsCryptAll.sEnterpriseId = MarkAnyConfig.ENTER_ID;		// [필수] 그룹아이디
            wdsCryptAll.sEnterpriseName = "교보 생명";					// [선택] 그룹명
            wdsCryptAll.sCompanyId = MarkAnyConfig.COMPANY_ID;			// [필수] 회사아이디
            wdsCryptAll.sCompanyName = "교보 생명";					// [선택] 회사명

            dec_rs = wdsCryptAll.iDecrypt();
            LOGGER.info("파일 복호화 결과 : {}", dec_rs);
        }catch (Exception e) {
            LOGGER.error("API ERROR comononDec", e.fillInStackTrace());
            dec_rs = 999999;
        }
        return dec_rs;
    }


    private static int waterEnc(String filePath, WDSCryptAll wdsCryptAll) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.ENCFILE_DIR + filePath;
            LOGGER.info("ENC... {}" , encFullPath);
            wdsCryptAll.sSourceFilePath = encFullPath;		// 일반 파일
            // 복호화 함수 호출
            dec_rs = wdsCryptAll.iEncrypt();
            LOGGER.info("파일 암호화 결과 : {}", dec_rs);
        }catch (Exception e) {
            LOGGER.error("API ERROR comononEnc", e.fillInStackTrace());
            dec_rs = 999999;
        }
        return dec_rs;
    }

    private static int comononEnc(String filePath, WDSCryptAll wdsCryptAll) {
        int dec_rs = 0;
        try{
            String encFullPath = MarkAnyConfig.DRMFILE_DIR + filePath;
            LOGGER.info("ENC... {}" , encFullPath);
            wdsCryptAll.sSourceFilePath = encFullPath;		// 일반 파일
            // 복호화 함수 호출
            dec_rs = wdsCryptAll.iEncrypt();
            LOGGER.info("파일 암호화 결과 : {}", dec_rs);
        }catch (Exception e) {
            LOGGER.error("API ERROR comononEnc", e.fillInStackTrace());
            dec_rs = 999999;
        }
        return dec_rs;
    }

    /**
     * 파일 복호화
     * @param filePath
     * @return
     */
    public static int toExtract(String filePath) {
        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        //복호화 경로
        wdsCryptAll.sDestFilePath = MarkAnyConfig.DRMFILE_DIR+ filePath;
        return comononDec(filePath, wdsCryptAll);
    }


    /**
     * 파일 복호화
     * @param filePath
     * @return
     */
    public static int extractToSource(String filePath) {
        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        wdsCryptAll.sDestFilePath = MarkAnyConfig.ENCFILE_DIR + filePath;
        return comononDec(filePath, wdsCryptAll);
    }

    /**
     * 파일 복호화 마크애니 설정 외 경로
     * @param filePath
     * @return
     */
    public static int extractToExternSource(String filePath, String fileFullPath) {
        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        wdsCryptAll.sDestFilePath = fileFullPath;
        LOGGER.info("externPath {}", fileFullPath);
        return comononDec(filePath, wdsCryptAll);
    }

    /**
     * 워터마크 해제
     * @param filePath
     * @return
     */
    public static int toWaterMark(String filePath, String userId, String fileName, String ext) {

        String decFullPath = MarkAnyConfig.ENCFILE_DIR + filePath;

        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        //복호화 경로
        wdsCryptAll.sDestFilePath = MarkAnyConfig.ENCFILE_DIR+ filePath;
        comononDec(filePath, wdsCryptAll);

        wdsCryptAll = new WDSCryptAll();

        wdsCryptAll.sUserId = userId;								// [필수] 사용자 아이디 또는 사번
        wdsCryptAll.sEnterpriseId = MarkAnyConfig.ENTER_ID;		// [필수] 그룹아이디
        wdsCryptAll.sEnterpriseName = "교보생명";					// [선택] 그룹명
        wdsCryptAll.sCompanyId = MarkAnyConfig.COMPANY_ID;			// [필수] 회사아이디
        wdsCryptAll.sCompanyName = "교보생명";					// [선택] 회사명
        wdsCryptAll.sDocId = "eSpider_" + LocalDateTime.now().format(timeFormatter);
        // [필수] 파일별 Unique해야함 (로그 분석 시 사용되는 키값)
        wdsCryptAll.sDocTitle = "DRM Doc Title";						// [선택] 문서 타이틀
        //wdsCryptAll.sDocGrade = "대외비";								// [선택] 문서 등급명

        wdsCryptAll.sEncryptedBy = "0";								// [필수] 암호화 파일 속성 (0:서버 암호화, 1:자동 암호화, 2:암호화 툴 사용)
        wdsCryptAll.sFileName = fileName;		// [필수] 파일명칭 (암호화 헤더에 들어가는 파일명)
        //wdsCryptAll.sDocCreatorId = "admin";							// [선택] 파일 최초 생성자 아이디
        //wdsCryptAll.sMachineKey = "";									// [선택] 서버 머신키
        wdsCryptAll.sServerOrigin = "ESPIDER";							// [필수] 서버명 또는 시스템 약어

        // [ACL 설정]
        wdsCryptAll.sDocExchangePolicy = "1";							// [필수] 문서 교환 정책 (0:개인한, 1:사내한, 2:부서한)
        //wdsCryptAll.sDocOfflinePolicy = "2";							// [선택] 오프라인 정책 (1:암호화 문서 사용불가, 2:암호화 문서 읽기 전용)
        wdsCryptAll.iDocOpenCount = -99;								// [필수] 오픈 횟수 (0~999 입력, -99 : 무제한)
        wdsCryptAll.iDocPrintCount = -99;								// [필수] 인쇄 횟수 (0~999 입력, -99 : 무제한)
        wdsCryptAll.iCanSave = 1;										// [필수] 저장 가능 여부 (0:저장 불가능, 1:저장 가능)
        wdsCryptAll.iCanEdit = 0;										// [필수] 편집 가능 여부 (0:편집 불가능, 1:편집 가능)
        wdsCryptAll.iClipboardOption = 0;								// [필수] 암호화 문서의 클립보드 제어(0:클립보드 제어, 1:제어하지 않음)
        wdsCryptAll.iVisiblePrint = 1;								// [필수] 워터마크 적용 여부 (0:적용 안함, 1:적용함)
        wdsCryptAll.iImageSafer = 0;									// [필수] 화면캡쳐 방지 여부 (1:방지, 0:허용 or 방지하지않음)
        wdsCryptAll.sDocValidPeriod = "-99";							// [필수] sValidPeriodType 값이 1이면 숫자형식, 값이 2이면 yyyymmddhhmmss
        wdsCryptAll.iDocOpenLog = 1;									// [필수] 오픈 시 로그 전송 (1:전송, 0:미전송)
        wdsCryptAll.iDocSaveLog = 1;									// [필수] 저장 시 로그 전송 (1:전송, 0:미전송)
        wdsCryptAll.iDocPrintLog = 1;									// [필수] 인쇄 시 로그 전송 (1:전송, 0:미전송)

        wdsCryptAll.sServerInfo_Log = "drm.donghee.co.kr:40002";		// [필수] 로그 전송 url:port
        wdsCryptAll.iOnlineAclControl = 0;							// [필수] 실시간 권한 제어(0:미사용, 1:사용)
        wdsCryptAll.sDocType = ext;									// [필수] 파일명 확장자
        //wdsCryptAll.sGroupId = "0";									// [선택] 그룹코드
        //wdsCryptAll.sGroupName = "그룹명";							// [선택] 그룹명
        //wdsCryptAll.sPositionId = "0";								// [선택] 직급코드
        //wdsCryptAll.sPositionName = "직급명";							// [선택] 직급명
        //wdsCryptAll.sUserName = "사용자";								// [선택] 사용자이름 (sUserId 아이디를 가지는 사용자 이름)
        //wdsCryptAll.sDeptID = "0";									// [선택/필수] 부서코드 (문서교환정책이 부서한인 경우 필수)
        //wdsCryptAll.sDeptName = "부서명";								// [선택] 부서명
        //wdsCryptAll.sPositionLevel = "";								// [선택] 직위(급) 등급
        wdsCryptAll.sSecurityLevel = "0";								// [필수] 보안 등급 코드 (동희산업의 경우 0:암호화, 1:대외비)
        wdsCryptAll.sSecurityLevelName = "암호화";					// [필수] 보안 등급명
        //wdsCryptAll.sMultiUserID = "";								// [선택] 다중 사용자 계정 (문서교환정책이 멀티 유저인 경우 사용)
        //wdsCryptAll.sMultiUserName = "";								// [선택] 다중 사용자 이름 (문서교환정책이 멀티 유저인 경우 사용)
//        wdsCryptAll.sCreatorName = "eSpider";							// [선택] 생성자 이름
        wdsCryptAll.sUsableAlways = "0";								// [선택] 항상 사용 가능 설정 값 (1:무조건 열람)
        wdsCryptAll.sValidPeriodType = "1";							// [필수] (1:sDocValidPeriod의 값을 숫자로 표기, 0:sDocValidPeriod의 값을 yyyymmddhhmmss로 표기)
        //wdsCryptAll.sCreatorCompanyId = "DONGHEE-765D-18B2-6E3D";		// [선택] 생성자 소속 회사 아이디 (ID는 마크애니에서 발급)
        //wdsCryptAll.sCreatorDeptId = "0";								// [선택] 생성자 소속 부서 ID
        //wdsCryptAll.sCreatorGroupId = "0";							// [선택] 생성자 소속 그룹 ID
        //wdsCryptAll.sCreatorPositionId = "0";							// [선택] 생성자 소속 직위(급) ID
        //wdsCryptAll.sReserved01 = "";									// [선택] 공백
        //wdsCryptAll.sReserved02 = "";									// [선택] 공백
        //wdsCryptAll.sReserved03 = "";									// [선택] 공백
        //wdsCryptAll.sReserved04 = "";									// [선택] 공백
        //wdsCryptAll.sReserved05 = "";									// [선택] 공백
        wdsCryptAll.iEncryptPrevCipher = 0;
        // [선택] 0:신버전 암호화, 1:구버전 암호화, 디폴트 : 신버전 암호화


        wdsCryptAll.sDocGrade = "PSW";

        //복호화 경로
        wdsCryptAll.sDestFilePath = decFullPath;


        return waterEnc(filePath, wdsCryptAll);
    }

    /**
     *
     * 결재자 확인 시 열기
     *
     * 설정 값
     * iDocOpenCount  오픈 횟수 (0~999 입력, -99 : 무제한)
       iDocPrintCount 인쇄 횟수 (0~999 입력, -99 : 무제한)
       iCanSave  저장 가능 여부 (0:저장 불가능, 1:저장 가능)
       iCanEdit  편집 가능 여부 (0:편집 불가능, 1:편집 가능)
       iImageSafer  화면캡쳐 방지 여부 (1:방지, 0:허용 or 방지하지않음)
       sDocGrade  문서 등급명 EZS
     *
     * @param filePath
     * @return
     */
    public static int toEncOpen(String filePath, String userId, String fileName, String ext) {
        WDSCryptAll wdsCryptAll = new WDSCryptAll();
        //복호화 경로
        wdsCryptAll.sDestFilePath = MarkAnyConfig.DRMFILE_DIR+ filePath;
        comononDec(filePath, wdsCryptAll);

        wdsCryptAll = new WDSCryptAll();
        wdsCryptAll.sUserId = userId;								// [필수] 사용자 아이디 또는 사번 //사용자 사번
        wdsCryptAll.sEnterpriseId = MarkAnyConfig.ENTER_ID;		// [필수] 그룹아이디
        wdsCryptAll.sEnterpriseName = "교보 생명";					// [선택] 그룹명
        wdsCryptAll.sCompanyId = MarkAnyConfig.COMPANY_ID;			// [필수] 회사아이디
        wdsCryptAll.sCompanyName = "교보 생명";					// [선택] 회사명

        wdsCryptAll.sDocId = "eSpider_" + LocalDateTime.now().format(timeFormatter);
        // [필수] 파일별 Unique해야함 (로그 분석 시 사용되는 키값)
        wdsCryptAll.sDocTitle = "DRM Doc Title";						// [선택] 문서 타이틀
        //wdsCryptAll.sDocGrade = "대외비";								// [선택] 문서 등급명
        wdsCryptAll.sServerOrigin = "ESPIDER";							// [필수] 서버명 또는 시스템 약어
        wdsCryptAll.sEncryptedBy = "0";								// [필수] 암호화 파일 속성 (0:서버 암호화, 1:자동 암호화, 2:암호화 툴 사용)
        wdsCryptAll.sFileName = fileName;		// [필수] 파일명칭 (암호화 헤더에 들어가는 파일명)
        //wdsCryptAll.sDocCreatorId = "admin";							// [선택] 파일 최초 생성자 아이디
        //wdsCryptAll.sMachineKey = "";									// [선택] 서버 머신키

        // [ACL 설정]
        wdsCryptAll.sDocExchangePolicy = "1";							// [필수] 문서 교환 정책 (0:개인한, 1:사내한, 2:부서한)
        //wdsCryptAll.sDocOfflinePolicy = "2";							// [선택] 오프라인 정책 (1:암호화 문서 사용불가, 2:암호화 문서 읽기 전용)
        wdsCryptAll.iClipboardOption = 0;								// [필수] 암호화 문서의 클립보드 제어(0:클립보드 제어, 1:제어하지 않음)
        wdsCryptAll.iVisiblePrint = 1;								    // [필수] 워터마크 적용 여부 (0:적용 안함, 1:적용함)
        wdsCryptAll.sDocValidPeriod = "-99";							// [필수] sValidPeriodType 값이 1이면 숫자형식, 값이 2이면 yyyymmddhhmmss
        wdsCryptAll.iDocOpenLog = 1;									// [필수] 오픈 시 로그 전송 (1:전송, 0:미전송)
        wdsCryptAll.iDocSaveLog = 1;									// [필수] 저장 시 로그 전송 (1:전송, 0:미전송)
        wdsCryptAll.iDocPrintLog = 1;									// [필수] 인쇄 시 로그 전송 (1:전송, 0:미전송)

        wdsCryptAll.sServerInfo_Log = "drm.donghee.co.kr:40002";		// [필수] 로그 전송 url:port
        wdsCryptAll.iOnlineAclControl = 0;							// [필수] 실시간 권한 제어(0:미사용, 1:사용)
        wdsCryptAll.sDocType = ext;									// [필수] 파일명 확장자
        //wdsCryptAll.sGroupId = "0";									// [선택] 그룹코드
        //wdsCryptAll.sGroupName = "그룹명";							// [선택] 그룹명
        //wdsCryptAll.sPositionId = "0";								// [선택] 직급코드
        //wdsCryptAll.sPositionName = "직급명";							// [선택] 직급명
        //wdsCryptAll.sUserName = "사용자";								// [선택] 사용자이름 (sUserId 아이디를 가지는 사용자 이름)
        //wdsCryptAll.sDeptID = "0";									// [선택/필수] 부서코드 (문서교환정책이 부서한인 경우 필수)
        //wdsCryptAll.sDeptName = "부서명";								// [선택] 부서명
        //wdsCryptAll.sPositionLevel = "";								// [선택] 직위(급) 등급
        wdsCryptAll.sSecurityLevel = "0";								// [필수] 보안 등급 코드 (동희산업의 경우 0:암호화, 1:대외비)
        wdsCryptAll.sSecurityLevelName = "암호화";					// [필수] 보안 등급명
        //wdsCryptAll.sMultiUserID = "";								// [선택] 다중 사용자 계정 (문서교환정책이 멀티 유저인 경우 사용)
        //wdsCryptAll.sMultiUserName = "";								// [선택] 다중 사용자 이름 (문서교환정책이 멀티 유저인 경우 사용)
//        wdsCryptAll.sCreatorName = "eSpider";							// [선택] 생성자 이름
        wdsCryptAll.sUsableAlways = "0";								// [선택] 항상 사용 가능 설정 값 (1:무조건 열람)
        wdsCryptAll.sValidPeriodType = "1";							// [필수] (1:sDocValidPeriod의 값을 숫자로 표기, 0:sDocValidPeriod의 값을 yyyymmddhhmmss로 표기)
        //wdsCryptAll.sCreatorCompanyId = "DONGHEE-765D-18B2-6E3D";		// [선택] 생성자 소속 회사 아이디 (ID는 마크애니에서 발급)
        //wdsCryptAll.sCreatorDeptId = "0";								// [선택] 생성자 소속 부서 ID
        //wdsCryptAll.sCreatorGroupId = "0";							// [선택] 생성자 소속 그룹 ID
        //wdsCryptAll.sCreatorPositionId = "0";							// [선택] 생성자 소속 직위(급) ID
        //wdsCryptAll.sReserved01 = "";									// [선택] 공백
        //wdsCryptAll.sReserved02 = "";									// [선택] 공백
        //wdsCryptAll.sReserved03 = "";									// [선택] 공백
        //wdsCryptAll.sReserved04 = "";									// [선택] 공백
        //wdsCryptAll.sReserved05 = "";									// [선택] 공백
        wdsCryptAll.iEncryptPrevCipher = 1;							// [선택] 0:신버전 암호화, 1:구버전 암호화, 디폴트 : 신버전 암호화

        wdsCryptAll.sDocGrade = "EZS";
        wdsCryptAll.iDocOpenCount = 1;
        wdsCryptAll.iDocPrintCount = 0;
        wdsCryptAll.iCanSave = 0;
        wdsCryptAll.iCanEdit = 0;
        wdsCryptAll.iImageSafer = 1;

        wdsCryptAll.sDestFilePath =  MarkAnyConfig.DRMFILE_DIR + filePath;

        return comononEnc(filePath, wdsCryptAll);
    }
}
