package com.solupia.markany.service;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by solupia on 2017. 4. 14..
 *
 */
public class MarkAnyDBService {

    private JdbcTemplate jdbcTemplate;

    public MarkAnyDBService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int execInsertAppCrrpc(String userId, String type, String startDate, String endDate, String aprvUserId) {
        return jdbcTemplate.update("{call INSERT_APP_CRRPC (?, ?, ?, ?, ?)}", userId, type, startDate, endDate, aprvUserId);
    }

    public int execDsAgentSetCcfvalue(String ccfield, String ccfValue, String ccfName, String userId) {
        return jdbcTemplate.update("{call DS_AGENT_SET_CCFVALUE (?, ?, ?, ?)}", ccfield, ccfValue, ccfName, userId);
    }

}
