import com.solupia.safepc.dto.*;
import com.solupia.safepc.service.SafePcService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * Created by solupia on 2017. 4. 5..
 */
public class NixMain {

    public static void main(String[] args) {


        try {
//            RequestApplyPolicy requestApplyPolicy = new RequestApplyPolicy();
//
//            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
//
//            requestApplyPolicy.setReqNo("REQ" + dtf.format(LocalDateTime.now()));
//            requestApplyPolicy.setUserID(args[0]);
//            requestApplyPolicy.setAppPersonId(args[0]);
//            requestApplyPolicy.setReqDT(LocalDateTime.now());
//            requestApplyPolicy.setAppDT(LocalDateTime.of(2017, Month.APRIL, 5, 4, 0));
//            requestApplyPolicy.setStartDT(LocalDateTime.of(2017, Month.APRIL, 2, 13, 0));
//            requestApplyPolicy.setEndDT(LocalDateTime.of(2017, Month.APRIL, 12, 18, 0));
//            requestApplyPolicy.setAppState(2);
//            requestApplyPolicy.setReqReason("req test");
//            requestApplyPolicy.setReqMedia(Arrays.asList("cdrw", "parallel", "serial","removable"));
//            requestApplyPolicy.setReqMediaRead(Arrays.asList("cdrw", "parallel", "serial","removable"));
////            requestApplyPolicy.setReqMediaBlock(Arrays.asList("removable"));
//            requestApplyPolicy.setIpLock(1);
//
//            System.out.println(requestApplyPolicy.toXml());
//
//            SafePcService safePcService = new SafePcService();
//
//            PcExecResult pcExecResult = safePcService.postApplyPolicy(requestApplyPolicy.toXml());
//
//            System.out.println("response : " + pcExecResult.getCode());
//
//
//            String userRole = safePcService.getUserRole(args[0]);
//
//            System.out.println("user Role : " + userRole);


            RequestPolicyUSB requestPolicyUSB = new RequestPolicyUSB();
            requestPolicyUSB.setPType(args[0]);
            requestPolicyUSB.setReqDeviceType("S");
            requestPolicyUSB.setReqMgmtKey(args[1]);
            SafePcService safePcService = new SafePcService();
            System.out.println(requestPolicyUSB.toXml());
            safePcService.getUsbPolicyInfos(requestPolicyUSB.toXml());

//            SafePcService safePcService = new SafePcService();
//
//            DeviceRegist deviceRegist = new DeviceRegist();
//            deviceRegist.setReqPersonId(args[0]);
//            deviceRegist.setDeviceNo(args[1]);
//            System.out.println(deviceRegist.returnToXml());
//
//            safePcService.postReturnSecuUsb(deviceRegist.returnToXml());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
