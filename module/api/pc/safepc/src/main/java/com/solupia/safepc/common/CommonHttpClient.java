package com.solupia.safepc.common;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by solupia on 2017. 2. 20..
 */
public class CommonHttpClient {

    private static Logger LOGGER  = LoggerFactory.getLogger(CommonHttpClient.class);

    public static HttpResponse commHttpGet(String fullUrl) throws IOException {
        HttpResponse execute = null;
        CloseableHttpClient httpClient = CommonHttpClient.createHttpClientAcceptsUntrustedCerts();
        HttpGet httpGet = new HttpGet(fullUrl);
        execute = httpClient.execute(httpGet);
        return execute;
    }

    public static HttpResponse commHttpPost(String fullUrl, HttpEntity httpEntity) throws IOException {
        HttpResponse httpResponse = null;
        CloseableHttpClient httpClient = CommonHttpClient.createHttpClientAcceptsUntrustedCerts();
        HttpPost httpPost = new HttpPost(fullUrl);
        httpPost.setEntity(httpEntity);
        httpResponse = httpClient.execute(httpPost);
        return httpResponse;
    }

    private static CloseableHttpClient createHttpClientAcceptsUntrustedCerts() {
        HttpClientBuilder b = HttpClientBuilder.create();
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    return true;
                }
            }).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());

        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();
        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        b.setConnectionManager( connMgr);
        CloseableHttpClient client = b.build();
        return client;
    }

}
