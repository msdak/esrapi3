package com.solupia.safepc.common;

import com.solupia.safepc.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 13..
 */
public class CommonXMLParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonXMLParser.class);

    public static UserRole parsingXML(String source) {
        UserRole userRole = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(UserRole.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            userRole = (UserRole) unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("GET RULL XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return userRole;
    }

    public static ResponsePcexec getPcExecResultFromXml(String source) {
        ResponsePcexec responsePcexec = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponsePcexec.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            responsePcexec = (ResponsePcexec) unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("PcExecResult XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return responsePcexec;
    }

    public static List<RemovalDeviceInfo> getRemovalDeviceInfo(String source) {
        ResponseDeviceInfo responsePcexec = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseDeviceInfo.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            responsePcexec = (ResponseDeviceInfo) unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("PcExecResult XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return responsePcexec.getRemovalDeviceInfos();
    }

    public static List<RemovalDevicePolicyInfo> getRemovalDevicePolicyInfo(String source) {
        ResponseDevicePolicyInfo responsePcexec = null;
        try {
            LOGGER.info("PcExecResult XML {}", source);
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseDevicePolicyInfo.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            responsePcexec = (ResponseDevicePolicyInfo) unmarshaller.unmarshal(new StringReader(source));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return responsePcexec.getRemovalDevicePolicyInfos();
    }

    public static ResponseReturnedUsb getRetunedUsb(String source) {
        ResponseReturnedUsb responseReturnedUsb = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ResponseReturnedUsb.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            responseReturnedUsb = (ResponseReturnedUsb) unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("getRetunedUsb Source XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return responseReturnedUsb;
    }
}
