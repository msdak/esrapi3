package com.solupia.safepc.common;

import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 4. 19..
 */
public class DateTimeFormat {
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter dateTimeFormatterHourmmssSSS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    public static DateTimeFormatter dateTimeFormatterHour = DateTimeFormatter.ofPattern("yyyy-MM-dd HH");
    public static DateTimeFormatter dateTimeFormatterHHmmss = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
}
