package com.solupia.safepc.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 4. 14..
 */
public class GenerateReqNo {

    private static final String REQ_NO_PREFIX = "REQ";

    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

    public static String getReqNo(){
        return dtf.format(LocalDateTime.now());
    }

}
