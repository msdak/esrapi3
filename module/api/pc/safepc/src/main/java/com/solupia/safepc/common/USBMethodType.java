package com.solupia.safepc.common;

/**
 * Created by solupia on 2017. 4. 19..
 */
public enum USBMethodType {
    PROPOSAL,
    TAKEOUT
}
