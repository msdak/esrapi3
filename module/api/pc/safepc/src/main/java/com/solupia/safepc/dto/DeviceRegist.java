package com.solupia.safepc.dto;

import com.solupia.safepc.common.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Getter @Setter
public class DeviceRegist {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceRegist.class);

    private String reqPersonId;
    private String appPersonId;
    private String reqNo;
    private LocalDateTime startDT;
    private LocalDateTime endDT;
    private String appState = "2";
    private String reqReason;
    private String deviceNo;
    private String pType;
    private String dType;
    private String possessType;
    private String useType;
    private String dSize;
    private String privateDiv;
    private String vendor = "";
    private LocalDateTime reqDT;
    private LocalDateTime appDT;

    //교보응2@
    protected String dateToString(LocalDateTime time) {
        if(time == null) return "";
        return DateTimeFormat.dateTimeFormatter.format(time);
    }

    /**
     * 웹서비스 통신을 위한 Request XML
     *
     * @return
     */
    public String toXml() {
        StringBuffer xml = new StringBuffer();
            xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
            xml.append("<request>\n");
            xml.append("<user></user>\n");
            xml.append("<password></password>\n");
            xml.append("<body>\n");
                xml.append("<command>postPublicDevice</command>\n");
                xml.append("<arguments>\n");
                    xml.append(String.format("<reqPersonId>%s</reqPersonId>\n", this.reqPersonId));
                    xml.append(String.format("<appPersonId>%s</appPersonId>\n", this.appPersonId));
                    xml.append(String.format("<reqNo>%s</reqNo>\n", this.getReqNo() == null ? "" : this.reqNo));
                    xml.append(String.format("<startDT>%s</startDT>\n", this.dateToString(this.startDT)));
                    xml.append(String.format("<endDT>%s</endDT>\n",this.dateToString(this.endDT)));
                    xml.append(String.format("<appState>%s</appState>\n", this.appState));
                    xml.append(String.format("<reqReason><![CDATA[%s]]></reqReason>\n", this.getReqReason()));
                    xml.append(String.format("<deviceNo><![CDATA[%s]]></deviceNo>\n", this.getDeviceNo()));
                    xml.append(String.format("<pType>%s</pType>\n", this.pType));
                    xml.append(String.format("<dType>%s</dType>\n", this.dType));
                    xml.append(String.format("<possessType>%s</possessType>\n", this.possessType));
                    xml.append(String.format("<useType>%s</useType>\n", this.useType));
                    xml.append(String.format("<dSize>%s</dSize>\n", this.dSize));
                    xml.append(String.format("<privateDiv>%s</privateDiv>\n", this.privateDiv));
                    xml.append(String.format("<vendor><![CDATA[%s]]></vendor>\n", ""));
                    xml.append(String.format("<reqDT>%s</reqDT>\n", DateTimeFormat.dateTimeFormatterHourmmssSSS.format(this.getReqDT())));
                    xml.append(String.format("<appDT>%s</appDT>\n", this.getAppDT() == null ? "" : DateTimeFormat.dateTimeFormatterHourmmssSSS.format(this.getAppDT())));
                xml.append("</arguments>\n");
                xml.append("</body>\n");
            xml.append("</request>");
        LOGGER.info("toXML {}", xml.toString());
        return xml.toString();
    }

    public String returnToXml() {
        StringBuffer xml = new StringBuffer();
        xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
        xml.append("<BODY>\n");
        xml.append(String.format("<REQUESTID>%s</REQUESTID>\n", this.reqPersonId));
        xml.append(String.format("<DEVICENO>%s</DEVICENO>\n", this.deviceNo));
        xml.append("</BODY>");
        LOGGER.info("returnToXml {}", xml.toString());
        return xml.toString();
    }
}
