package com.solupia.safepc.dto;

import com.solupia.safepc.common.DateTimeFormat;
import com.solupia.safepc.common.GenerateReqNo;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by solupia on 2017. 4. 19..
 */
@Getter
@Setter
public class ExternDeviceRegist extends DeviceRegist {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternDeviceRegist.class);

    private String reqGroupId;
    private String mgmtKey;
    private String usePurpose;
    private String copyProtect;
    private String ipFilter;
    private String pwRestrictionCnt;
    private String deviceRW;

    /**
     * 웹서비스 통신을 위한 Request XML
     *
     * @return
     */
    @Override
    public String toXml() {
        StringBuffer xml = new StringBuffer();
        xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
        xml.append("<request>\n");
        xml.append("<user></user>\n");
        xml.append("<password></password>\n");
        xml.append("<body>\n");
        xml.append("<command>SecOutDevice</command>\n");
        xml.append("<arguments>\n");
        xml.append(String.format("<ReqPersonId>%s</ReqPersonId>\n", this.getReqPersonId()));
        xml.append(String.format("<ReqGroupId>%s</ReqGroupId>\n", this.reqGroupId));
        xml.append(String.format("<AppPersonId>%s</AppPersonId>\n", this.getAppPersonId()));
        xml.append(String.format("<ReqDT>%s</ReqDT>\n", DateTimeFormat.dateTimeFormatterHHmmss.format(this.getReqDT())));
        xml.append(String.format("<AppDT>%s</AppDT>\n", this.getAppDT() == null ? "" : DateTimeFormat.dateTimeFormatterHHmmss.format(this.getAppDT())));
//        xml.append(String.format("<ReqNo>%s</ReqNo>\n", this.getReqNo() == null ? "" : this.getReqNo()));
        xml.append(String.format("<ReqNo>%s</ReqNo>\n", GenerateReqNo.getReqNo()));
        xml.append(String.format("<StartDT>%s</StartDT>\n", this.dateToString(this.getStartDT())));
        xml.append(String.format("<EndDT>%s</EndDT>\n",this.dateToString(this.getEndDT())));
        xml.append(String.format("<AppState>2</AppState>\n", this.getAppState()));
        xml.append(String.format("<ReqReason><![CDATA[%s]]></ReqReason>\n", this.getReqReason()));
        xml.append(String.format("<mgmtKey>%s</mgmtKey>\n", this.mgmtKey));
        xml.append(String.format("<UsePurpose>%s</UsePurpose>\n", this.usePurpose));
        xml.append(String.format("<COPY_PROTECT>%s</COPY_PROTECT>\n", this.copyProtect));
        xml.append(String.format("<IP_FILTER>%s</IP_FILTER>\n", this.ipFilter));
        xml.append(String.format("<PW_RESTRICTION_CNT>%s</PW_RESTRICTION_CNT>\n", this.pwRestrictionCnt));
        xml.append(String.format("<DeviceRW>%s</DeviceRW>\n", this.deviceRW));
        xml.append("</arguments>\n");
        xml.append("</body>\n");
        xml.append("</request>");
        LOGGER.info("toXML {}", xml.toString());
        return xml.toString();
    }
}
