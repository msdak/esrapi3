package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by solupia on 2017. 4. 17.
 *
 * <result code="0" message="성공"/>
 * <result ReqNo="REQREQ20141126114127327" message="ReqNo값"/>
 *
 */
@XmlRootElement
@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class PcControlResult {

    @XmlAttribute(name = "code")
    private String code;
    @XmlAttribute(name = "ReqNo")
    private String reqNo;
    @XmlAttribute(name = "message")
    private String message;
}
