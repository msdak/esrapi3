package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by solupia on 2017. 4. 18..
 */
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class RemovalDeviceInfo {

    @XmlElement(name = "deviceNo")
    private String deviceNo;
    @XmlElement(name = "mgmtNo")
    private String mgmtNo;
    @XmlElement(name = "mgmtKey")
    private String mgmtKey;
    @XmlElement(name = "dSize")
    private String dSize;
    @XmlElement(name = "userID")
    private String userId;
    @XmlElement(name = "serialNo")
    private String serialNo;

}