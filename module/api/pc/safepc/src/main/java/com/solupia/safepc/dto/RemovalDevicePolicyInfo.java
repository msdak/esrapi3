package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by solupia on 2017. 4. 18..
 */
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class RemovalDevicePolicyInfo {

    @XmlElement(name = "deviceNo")
    private String deviceNo;
    @XmlElement(name = "mgmtNo")
    private String mgmtNo;
    @XmlElement(name = "mgmtKey")
    private String mgmtKey;
    @XmlElement(name = "out_startDT")
    private String outStartDT;
    @XmlElement(name = "out_endDT")
    private String outEndDT;
    @XmlElement(name = "out_deviceRW")
    private String outDeviceRW;
    @XmlElement(name = "out_copy_Protect")
    private String outCopyProtect;
    @XmlElement(name = "out_ip_Filter")
    private String outIpFilter;
    @XmlElement(name = "out_pw_restriction_Cnt")
    private String outPwRestrictionCnt;
}