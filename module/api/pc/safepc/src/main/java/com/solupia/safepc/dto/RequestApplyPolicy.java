package com.solupia.safepc.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.solupia.safepc.common.DateTimeFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Created by solupia on 2017. 4. 5..
 */
@Setter @Getter
public class RequestApplyPolicy {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
    private DateTimeFormatter dateTimeFormatterHour = DateTimeFormatter.ofPattern("YYYY-MM-dd HH");

    private String reqNo = "";
    private String userID;
    private String appPersonId;
    private LocalDateTime reqDT;
    private LocalDateTime appDT;
    //내부:INT_REQ, 외부:EXT_REQ(내부미디어 요청/승인, 노트북 외부반출 요청/승인)
    private String reqGubun = "INT_REQ";
    private LocalDateTime startDT;
    private LocalDateTime endDT;
    //0:대기, 1:진행, 2:승인, 3:부결, 4:신청_취소, 5:등록완료
    private int appState = 2;
    private String reqReason;

//    "fdd: FDD
//    cdrw: CD/DVD
//    serial: 시리얼포트
//    removable: 휴대용저장장치
//    parallel: 패럴럴포트
//    wlan: 무선랜
//    pda: PDA
//    red: 적외선포트
//    virtual: 가상포트
//    hsdpa: HSDPA
//    wibro: WIBRO
//    hsdpa:HSDPA
//    smartphone: 스마트폰
//    tethering: 테더링"	"매체값이 여러 개면
//    <reqMedia>fdd</reqMedia>
//    <reqMedia>removable</reqMedia>
//    식으로 보내면 됩니다."
    private List<String> reqMedia;
    private List<String> reqMediaRead;
    private List<String> reqMediaBlock;

    //"0 : 사용  1 : 사용 안함"
    private int  ipLock = 1;

    private String listToStr(List<String> source, String tag) {
        StringBuffer reqMediaStr = new StringBuffer();
        if(Objects.isNull(source)){
            reqMediaStr.append("");
        } else {
            source.forEach(s -> reqMediaStr.append(String.format("<%s>%s</%s>\n", tag, s, tag)));
        }
        return reqMediaStr.toString();
    }

    /**
     * 웹서비스 통신을 위한 Request XML
     *
     * @return
     */
    public String toXml() {
        StringBuffer xml = new StringBuffer();
            xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
            xml.append("<request>\n");
            xml.append("<user></user>\n");
            xml.append("<password></password>\n");
                xml.append("<body>\n");
                xml.append("<command>SetPolicy</command>\n");
                    xml.append("<arguments>\n");
                        xml.append(String.format("<reqNo>%s</reqNo>\n", this.reqNo));
                        xml.append(String.format("<userID>%s</userID>\n", this.userID));
                        xml.append(String.format("<appPersonId>%s</appPersonId>\n", this.appPersonId));
                        xml.append(String.format("<reqDT>%s</reqDT>\n", dateTimeFormatter.format(this.reqDT)));
                        xml.append(String.format("<appDT>%s</appDT>\n", dateTimeFormatter.format(this.appDT)));
                        xml.append(String.format("<reqGubun>%s</reqGubun>\n", this.reqGubun));
                        //API 설명서랑 조금 다름 yyyy-MM-dd HH:mm:ss 욧렇게 가야함
                        xml.append(String.format("<startDT>%s</startDT>\n", DateTimeFormat.dateTimeFormatterHHmmss.format(this.startDT)));
                        xml.append(String.format("<endDT>%s</endDT>\n", DateTimeFormat.dateTimeFormatterHHmmss.format(this.endDT)));
                        //API 설명서랑 조금 다름
                        xml.append(String.format("<appState>%s</appState>\n", this.appState));
                        xml.append(String.format("<reqReason><![CDATA[%s]]></reqReason>\n", this.reqReason));
                        xml.append(this.listToStr(this.reqMedia, "reqMedia"));
                        xml.append(this.listToStr(this.reqMediaRead, "reqMedia_Read"));
                        xml.append(this.listToStr(this.reqMediaBlock, "reqMedia_Block"));
                        xml.append(String.format("<ipLock>%s</ipLock>\n", this.ipLock));
                    xml.append("</arguments>\n");
                xml.append("</body>\n");
            xml.append("</request>");
        return xml.toString();
    }
}
