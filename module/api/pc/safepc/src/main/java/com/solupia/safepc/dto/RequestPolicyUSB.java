package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 보안 USB 반출 상태를 조회 하는 DTO
 */
@Getter @Setter
public class RequestPolicyUSB extends RequestUSB {

    private String pType;

    public String toXml() {
        StringBuffer xml = new StringBuffer();
        xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
        xml.append("<request>\n");
        xml.append("<user></user>\n");
        xml.append("<password></password>\n");
        xml.append("<body>\n");
        xml.append("<command>getPolicyDevice</command>\n");
        xml.append("<arguments>\n");
        xml.append(String.format("<pType>%s</pType>\n", this.pType));
        xml.append(String.format("<reqDeviceType>%s</reqDeviceType>\n", super.getReqDeviceType()));
        xml.append(String.format("<reqMgmtKey>%s</reqMgmtKey>\n", super.getReqMgmtKey()));
        xml.append("</arguments>\n");
        xml.append("</body>\n");
        xml.append("</request>\n");
        return xml.toString();
    }

}
