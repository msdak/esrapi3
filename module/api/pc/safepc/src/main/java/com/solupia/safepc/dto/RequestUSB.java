package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 4. 17..
 */
@Getter @Setter
public class RequestUSB {

    private String reqType;
    private String reqDeviceType;
    private String reqPersonId;
    private String reqGroupId;
    private String reqMgmtKey;
    private String deviceKey;

    public String toXml() {
        StringBuffer xml = new StringBuffer();
        xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
        xml.append("<request>\n");
            xml.append("<user></user>\n");
            xml.append("<password></password>\n");
            xml.append("<body>\n");
                xml.append("<command>getUniteDevice</command>\n");
                xml.append("<arguments>\n");
                xml.append(String.format("<reqType>%s</reqType>\n", this.reqType));
                xml.append(String.format("<reqDeviceType>%s</reqDeviceType>\n", this.reqDeviceType));
                xml.append(String.format("<reqPersonId>%s</reqPersonId>\n", this.reqPersonId));
                xml.append(String.format("<reqGroupId>%s</reqGroupId>\n", this.reqGroupId));
                xml.append(String.format("<reqMgmtKey>%s</reqMgmtKey>\n", this.reqMgmtKey));
                xml.append("</arguments>\n");
            xml.append("</body>\n");
        xml.append("</request>\n");
        return xml.toString();
    }

    public String usbReturnedToXml() {
        StringBuffer xml = new StringBuffer();
        xml.append("<?xml version='1.0' encoding='utf-8'?>\n");
        xml.append("<BODY>\n");
            xml.append(String.format("<REQUESTID>%s</REQUESTID>\n", this.reqPersonId));
            xml.append(String.format("<DEVICENO>%s</DEVICENO>\n", this.deviceKey));
        xml.append("</BODY>\n");
        return xml.toString();
    }
}

//
//    <?xml version='1.0' encoding='utf-8'?>
//<request>
//<user>연동할때ID</user>
//<password>연동할때PWD</password>
//<body>
//<command>getUniteDevice</command>
//<arguments>
//<reqType>1</reqType>
//<reqDeviceType>N</reqDeviceType>
//<reqPersonId>user2</reqPersonId>
//<reqGroupId>testGroup</reqGroupId>
//<reqMgmtKey>20101223163348001</reqMgmtKey>
//</arguments>
//</body>
//</request>
