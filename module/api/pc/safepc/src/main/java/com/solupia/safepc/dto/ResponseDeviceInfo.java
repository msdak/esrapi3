package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 18..
 */
@Getter
@Setter
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDeviceInfo {

    @XmlElement(name = "user")
    private String user;

    @XmlElement(name = "body")
    private DeviceInfoBody body;

    public List<RemovalDeviceInfo> getRemovalDeviceInfos() {
        return body.getRemovalDeviceInfos();
    }

}

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "body")
@Getter @Setter
class DeviceInfoBody {

    @XmlElement(name = "command")
    private String command;

    @XmlElement(name = "arguments")
    private List<RemovalDeviceInfo> removalDeviceInfos;
}
