package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 18..
 */
@Getter
@Setter
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDevicePolicyInfo {

    @XmlElement(name = "user")
    private String user;

    @XmlElement(name = "body")
    private DevicePolicyInfoBody body;

    public List<RemovalDevicePolicyInfo> getRemovalDevicePolicyInfos() {
        return body.getRemovalDevicePolicyInfos();
    }

}

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "body")
@Getter @Setter
class DevicePolicyInfoBody {

    @XmlElement(name = "command")
    private String command;

    @XmlElement(name = "arguments")
    private List<RemovalDevicePolicyInfo> removalDevicePolicyInfos;
}
