package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 14..
 */
@XmlRootElement(name = "arguments")
@Getter @Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponsePcexec {

    @XmlElement(name = "result")
    List<PcExecResult> result;
}
