package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@XmlRootElement(name = "RESPONSE")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseReturnedUsb {

    @XmlElement(name = "ERRORCODE")
    public String errorCode;

    @XmlElement(name = "ERRORMSG")
    public String errorMsg;
}
