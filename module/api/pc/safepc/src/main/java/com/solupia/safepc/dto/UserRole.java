package com.solupia.safepc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 13..
 *
 * YY : 쓰기/읽기, NN : 차단 , YN : 쓰기, NY : 읽기, Y : 허용 , N : 차단
 */
@Getter @Setter
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserRole {

    @XmlElement(name = "user")
    private String user;

    @XmlElement(name = "body")
    private Body body;

    public List<Param> getParam() {
        return this.body.getUserROLEArguments().getParam();
    }

}


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "body")
@Getter @Setter
class Body {

    @XmlElement(name = "command")
    private String command;

    @XmlElement(name = "arguments")
    private UserROLEArguments userROLEArguments;
}

@Getter
@Setter
@XmlRootElement(name = "arguments")
@XmlAccessorType(XmlAccessType.FIELD)
class UserROLEArguments {
    List<Param> param;
}
