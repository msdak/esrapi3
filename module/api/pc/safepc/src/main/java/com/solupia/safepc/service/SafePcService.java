package com.solupia.safepc.service;

import com.solupia.safepc.common.CommonHttpClient;
import com.solupia.safepc.common.CommonXMLParser;
import com.solupia.safepc.common.USBMethodType;
import com.solupia.safepc.config.SafePcConfig;
import com.solupia.safepc.dto.PcExecResult;
import com.solupia.safepc.dto.RemovalDeviceInfo;
import com.solupia.safepc.dto.RemovalDevicePolicyInfo;
import com.solupia.safepc.dto.ResponseReturnedUsb;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 5..
 */
public class SafePcService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SafePcService.class);

    private static String APPLY_POLICY_URL = "/GroupWare/postAppPolicy.nics";
    private static String USER_ROLE_URL = "/GroupWare/SetUserRule.asp?UserId=%s&inoutType=1";
    private static String USB_DEVICE_REQUEST_URL = "/GroupWare/postPublicDeviceRegist.nics";
    private static String TAKEOUT_USB_DEVICE_REQUEST_URL = "/GroupWare/postSecOutDevice.nics";

    private static String USB_GET_DEVICE_REQUEST_URL = "/GroupWare/getUniteDeviceList.nics";
    private static String USB_GET_POLICY_REQUEST_URL = "/GroupWare/getPolicyDevice.nics";

    private static final String USB_RETURN_URL = "/GroupWare/migSecureDeviceRetrieveServiceServlet.nics";

    /**
     * 매체 제어 API Call Method
     * @param xml
     * @return
     * @throws IOException
     */
    public PcExecResult postApplyPolicy(String xml) throws IOException {
        String url = SafePcConfig.REQUEST_URL + APPLY_POLICY_URL;
        String s = EntityUtils.toString(CommonHttpClient.commHttpPost(url, new StringEntity(xml)).getEntity());
        return CommonXMLParser.getPcExecResultFromXml(s).getResult().get(0);
    }

    /**
     * 정책 조회 API Call Method
     * @param userId
     * @return
     * @throws IOException
     */
    public String getUserRole(String userId) throws IOException {
        String url = SafePcConfig.REQUEST_URL + String.format(USER_ROLE_URL, userId);
        return EntityUtils.toString(CommonHttpClient.commHttpGet(url).getEntity());
    }

    /**
     * USB 사용 신청을 위한 API Call Method
     * @param xml
     * @return
     * @throws IOException
     */
    public List<PcExecResult> postUSBDeviceProposal(String xml, USBMethodType usbMethodType) throws IOException {
        String url = SafePcConfig.REQUEST_URL;
        switch (usbMethodType){
            case PROPOSAL:
                url += USB_DEVICE_REQUEST_URL;
                break;
            case TAKEOUT:
                url += TAKEOUT_USB_DEVICE_REQUEST_URL;
                break;
        }
        String s = EntityUtils.toString(CommonHttpClient.commHttpPost(url, new StringEntity(xml)).getEntity());
        return CommonXMLParser.getPcExecResultFromXml(s).getResult();
    }

    /**
     * 보안 USB 회수 API CALL
     * @param xml
     * @throws IOException
     */
    public ResponseReturnedUsb postReturnSecuUsb(String xml) throws IOException {
        String url = SafePcConfig.REQUEST_URL + USB_RETURN_URL;
        String s = EntityUtils.toString(CommonHttpClient.commHttpPost(url, new StringEntity(xml)).getEntity());
        LOGGER.info("return usb to string {}", s);
        return CommonXMLParser.getRetunedUsb(s);
    }

    public List<RemovalDeviceInfo> getUsbDeviceInfos(String xml) throws IOException {
        String url = SafePcConfig.REQUEST_URL + USB_GET_DEVICE_REQUEST_URL;
        String s = EntityUtils.toString(CommonHttpClient.commHttpPost(url, new StringEntity(xml)).getEntity());
        return CommonXMLParser.getRemovalDeviceInfo(s);
    }

    public List<RemovalDevicePolicyInfo> getUsbPolicyInfos(String xml) throws IOException {
        String url = SafePcConfig.REQUEST_URL + USB_GET_POLICY_REQUEST_URL;
        String s = EntityUtils.toString(CommonHttpClient.commHttpPost(url, new StringEntity(xml)).getEntity());
        return CommonXMLParser.getRemovalDevicePolicyInfo(s);
    }

    public void releaseNetworkLock(String auditUser, String userId) throws IOException {
        //audit_user -> 실행 요청자 id , user_id -> 계정 아이디
        String fullUrl = SafePcConfig.NAC_REQUEST_URL + String.format("/webapi/UserMgmt/setUserStatus.do?audit_user=%s&user_id=%s", auditUser, userId);
        String fullUrl2 = SafePcConfig.NAC_REQUEST_URL2 + String.format("/webapi/UserMgmt/setUserStatus.do?audit_user=%s&user_id=%s", auditUser, userId);
        CommonHttpClient.commHttpGet(fullUrl);
        CommonHttpClient.commHttpGet(fullUrl2);
    }

//  다중 파일 압축일경우 아래와 같이
//  MKSZipEx /s:d:\mkzip_test\data\test1.txt*d:\mkzip_test\data\test2.txt*d:\mkzip_test\data\test3.txt*d:\mkzip_test\data\test4.txt*d:\mkzip_test\data\test5.txt /d:d:\mkzip_test\zip\test.zip /e:et1.txt*et2.txt*et3.txt*et4.txt*et5.txt /i:dev5 /p:1q2w3e4r /t:3
//  [압축파일 복사 용도 - 4:일반USB ,8:인증USB , 16:보안USB, 256:네트워크 (복수 선택이 가능하며 합산 된 값이 들어가면 됩니다.)];
    public void takeoutFile(String source, String target, String orgFilename, String userId, String binPath) throws IOException {
        String[] command = new String[3];
        command[0] = "cmd.exe";
        command[1] = "/c";
        command[2] = binPath +"MkSZipEx.exe /s:\""+ source +"\" /d:\""+ target +"\" /e:\"\" /i:\""+ userId +"\" /p:\"\" /t:\"16\"";
        //String command = "cmd.exe /c "+ binPath +"MkSZipEx.exe /s:\""+ source +"\" /d:\""+ target +"\" /e:\"\" /i:\""+ userId +"\" /p:\"\" /t:\"16\"";
        LOGGER.info("takeout_command : {}", command);
        Process exec = Runtime.getRuntime().exec(command);
        BufferedReader br = new BufferedReader(new InputStreamReader(exec.getInputStream()));
        String sReturn = "";
        String line;
        while ((line = br.readLine()) != null) {
            sReturn += line;
        }
    }
}
