package com.solupia.safepc.common;

import com.solupia.safepc.dto.*;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Created by solupia on 2017. 4. 13..
 */
public class CommonXMLParserTest {

    @Test
    public void parsingXML() throws Exception {

        String source = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<request>\n" +
                    "<user>연동할때ID</user>\n" +
                    "<password>연동할때PWD</password>\n" +
                    "<body>\n" +
                        "<command>SetUserRule</command>\n" +
                        "<arguments>\n" +
                            "<param name=\"UserId\" value=\"user2\"/>" +
                            "<param name=\"fdd\" value=\"NY\"/>" +
                            "<param name=\"cdrw\" value=\"NY\"/>" +
                            "<param name=\"serial\" value=\"Y\"/>" +
                            "<param name=\"removable\" value=\"NN\"/>\n" +
                        "</arguments>\n" +
                    "</body>\n" +
                "</request>";
        CommonXMLParser commonXMLParser = new CommonXMLParser();
        UserRole userRole = commonXMLParser.parsingXML(source);

        System.out.println(userRole.getParam().size());
//        System.out.println(userRole.getParam().size());
    }

    @Test
    public void test_value() {
        String source = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<request>\n" +
                "<user>연동할때ID</user>\n" +
                "<password>연동할때PWD</password>\n" +
                "<body>\n" +
                "<command>SetUserRule</command>\n" +
                "<arguments>\n" +
                "<param name=\"UserId\" value=\"user2\"/>" +
                "<param name=\"fdd\" value=\"NY\"/>" +
                "<param name=\"cdrw\" value=\"NY\"/>" +
                "<param name=\"serial\" value=\"Y\"/>" +
                "<param name=\"removable\" value=\"NN\"/>\n" +
                "<param name=\"ipLock\" value=\"Y\"/>\n" +
                "</arguments>\n" +
                "</body>\n" +
                "</request>";
        CommonXMLParser commonXMLParser = new CommonXMLParser();
        UserRole userRole = commonXMLParser.parsingXML(source);
        //cdrw : w
        Set<String> reqMedias = new LinkedHashSet<>();
        Set<String> reqMeReaddias = new LinkedHashSet<>();
        Set<String> reqMeBlockd = new LinkedHashSet<>();
        String userId = null;
        for(Param param : userRole.getParam()){
            if(param.getName().equals("UserId")) {
                userId = param.getValue();
                continue;
            }
            String t = param.getValue();
            String name = param.getName();
            if(isDoubleType(t.length())) {
                if(t.charAt(0) == 'N' && t.charAt(1) == 'N') {
                    reqMeBlockd.add(name);
                    continue;
                }
                if(t.charAt(0) == 'Y') reqMedias.add(name);
                if(t.charAt(1) == 'Y') reqMeReaddias.add(name);
            }else {
                if(t.charAt(0) == 'Y') reqMedias.add(name);
                else if(t.charAt(0) == 'N') reqMeBlockd.add(name);
            }
        }

        List<Param> requestParam = new LinkedList<>();
        Param param1 = new Param();
        param1.setName("removable");
        requestParam.add(param1);

        //TODO request polycy 만들기
        for(Param param : requestParam) {
            reqMeBlockd.remove(param.getName());
            reqMedias.add(param.getName());
            reqMeReaddias.add(param.getName());
        }

        RequestApplyPolicy requestApplyPolicy = new RequestApplyPolicy();
        requestApplyPolicy.setReqNo("REQyyyyMMddHHmmssSSS");
        requestApplyPolicy.setUserID(userId);
        requestApplyPolicy.setAppPersonId("apprvId");
        requestApplyPolicy.setReqDT(LocalDateTime.now());
        requestApplyPolicy.setAppDT(LocalDateTime.of(2017, Month.JUNE, 5, 4, 0));
        requestApplyPolicy.setStartDT(LocalDateTime.of(2017, Month.MAY, 2, 23, 0));
        requestApplyPolicy.setEndDT(LocalDateTime.of(2017, Month.JULY, 3, 23, 0));
        requestApplyPolicy.setAppState(2);
        requestApplyPolicy.setReqReason("req test");
        requestApplyPolicy.setReqMedia(reqMedias.stream().collect(Collectors.toList()));
        requestApplyPolicy.setReqMediaRead(reqMeReaddias.stream().collect(Collectors.toList()));
        requestApplyPolicy.setReqMediaBlock(reqMeBlockd.stream().collect(Collectors.toList()));
        requestApplyPolicy.setIpLock(1);

        System.out.println(requestApplyPolicy.toXml());
        System.out.println("---------------------------------------------------");
    }

    private boolean isDoubleType(int length) {
        return length == 2;
    }

    @Test
    public void test_getDeviceResultFromXml() {
        String source = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<arguments>\n" +
                    "<result code=\"0\" message=\"성공\"/>\n" +
                    "<result ReqNo=\"REQREQ20141126114127327\" message=\"ReqNo값\"/>\n" +
                    "<result mgmtKey=\"20101223163348001\" message=\"mgmtKey값\"/>" +
                "</arguments>\n";
        ResponsePcexec deviceResultFromXml = CommonXMLParser.getPcExecResultFromXml(source);

        System.out.println(deviceResultFromXml.getResult());
    }


    @Test
    public void test_getApply() {
        String s = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<arguments>\n" +
                "<result code=\"0\" message=\"성공\"/>\n" +
                "<result ReqNo=\"REQREQ20141126114127327\" message=\"ReqNo값\"/>\n" +
                "</arguments>\n";

        ResponsePcexec pcControlResult = CommonXMLParser.getPcExecResultFromXml(s);

        System.out.println(pcControlResult.getResult().get(1).getReqNo());
    }

    @Test
    public void test_getUsbDeviceInfos() {
        String s = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<request>\n" +
                "<user>연동할때ID</user>\n" +
                "<password>연동할때PWD</password>\n" +
                "<body>\n" +
                "<command>getUniteDevice</command>\n" +
                "<arguments_cnt>2</arguments_cnt>\n" +
                "<arguments>\n" +
                    "<deviceNo><![CDATA[EC14-EC32-3500-007B-0E76-999F]]></deviceNo>\n" +
                    "<mgmtNo><![CDATA[채널보안팀-II급-001]]></mgmtNo>\n" +
                    "<mgmtKey><![CDATA[20141127102233001]]></mgmtKey>\n" +
                    "<dSize>1024</dSize>\n" +
                    "<userID>testid234</userID>\n" +
                "</arguments>\n" +
                "<arguments>\n" +
                    "<deviceNo><![CDATA[EC14-EC32-3500-007B-0E76-999F]]></deviceNo>\n" +
                    "<mgmtNo><![CDATA[채널보안팀-II급-001]]></mgmtNo>\n" +
                    "<mgmtKey><![CDATA[20141127102233001]]></mgmtKey>\n" +
                    "<serialNo><![CDATA[사이트에서 수동생성]]></serialNo>\n" +
                    "<dSize>2048</dSize>\n" +
                    "<userID>testid</userID>\n" +
                "</arguments>\n" +
                "</body>\n" +
                "</request>";
        List<RemovalDeviceInfo> removalDeviceInfo = CommonXMLParser.getRemovalDeviceInfo(s);

        System.out.println(removalDeviceInfo);
    }

    @Test
    public void test_getUsbDevicePolicy() {
        String s = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<request>\n" +
                "<user>연동할때ID</user>\n" +
                "<password>연동할때PWD</password>\n" +
                "<body>\n" +
                "<command>getPolicyDevice</command>\n" +
                "<arguments>\n" +
                "<deviceNo><![CDATA[EC14-EC32-3500-007B-0E76-999F]]></deviceNo>\n" +
                "<mgmtNo><![CDATA[채널보안팀-II급-001]]></mgmtNo>\n" +
                "<mgmtKey><![CDATA[20141127102233001]]></mgmtKey>\n" +
                "<out_startDT>1024</out_startDT>\n" +
                "<out_endDT>testid234</out_endDT>\n" +
                "<out_deviceRW>testid234</out_deviceRW>\n" +
                "<out_copy_Protect>N</out_copy_Protect>\n" +
                "<out_ip_Filter></out_ip_Filter>\n" +
                "<out_pw_restriction_Cnt>30</out_pw_restriction_Cnt>\n" +
                "</arguments>\n" +
                "</body>\n" +
                "</request>";
        List<RemovalDevicePolicyInfo> removalDevicePolicyInfos = CommonXMLParser.getRemovalDevicePolicyInfo(s);

        System.out.println(removalDevicePolicyInfos);
    }

    @Test
    public void test_getReturnedUSB() {
        String s = "<?xml version='1.0' encoding='utf-8'?>\n"+
                "<RESPONSE>\n" +
                "<ERRORCODE>0</ERRORCODE><ERRORMSG>OK</ERRORMSG>\n" +
                "</RESPONSE>";
        ResponseReturnedUsb responseReturnedUsb = CommonXMLParser.getRetunedUsb(s);

        System.out.println(responseReturnedUsb.getErrorCode());
        System.out.println(responseReturnedUsb.getErrorMsg());

        s = "<?xml version='1.0' encoding='utf-8'?>\n" +
                "<RESPONSE>\n" +
                "<ERRORCODE>500</ERRORCODE><ERRORMSG>요청데이터에 오류가 있습니다.(1)</ERRORMSG>\n" +
                "</RESPONSE>";

        responseReturnedUsb = CommonXMLParser.getRetunedUsb(s);
        System.out.println(responseReturnedUsb.getErrorCode());
        System.out.println(responseReturnedUsb.getErrorMsg());
    }
}