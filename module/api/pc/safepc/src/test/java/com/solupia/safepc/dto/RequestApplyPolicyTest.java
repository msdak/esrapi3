package com.solupia.safepc.dto;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * Created by solupia on 2017. 4. 5..
 */
public class RequestApplyPolicyTest {

    @Test
    public void test_toXML() {
        RequestApplyPolicy requestApplyPolicy = new RequestApplyPolicy();

        requestApplyPolicy.setReqNo("REQyyyyMMddHHmmssSSS");
        requestApplyPolicy.setUserID("testID");
        requestApplyPolicy.setAppPersonId("apprvId");
        requestApplyPolicy.setReqDT(LocalDateTime.now());
        requestApplyPolicy.setAppDT(LocalDateTime.of(2017, Month.JUNE, 5, 4, 0));
        requestApplyPolicy.setStartDT(LocalDateTime.of(2017, Month.MAY, 2, 23, 0));
        requestApplyPolicy.setEndDT(LocalDateTime.of(2017, Month.JULY, 3, 23, 0));
        requestApplyPolicy.setAppState(2);
        requestApplyPolicy.setReqReason("req test");
        requestApplyPolicy.setReqMedia(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setReqMediaRead(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setReqMediaBlock(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setIpLock(1);

        System.out.println(requestApplyPolicy.toXml());
        System.out.println("---------------------------------------------------");

        requestApplyPolicy.setReqMedia(Arrays.asList("fdd"));
        requestApplyPolicy.setReqMediaRead(Arrays.asList("fdd"));
        requestApplyPolicy.setReqMediaBlock(Arrays.asList("fdd"));

        System.out.println(requestApplyPolicy.toXml());

    }

}