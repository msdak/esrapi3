package com.solupia.safepc.dto;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequestPolicyUSBTest {

    @Test
    public void toXml() throws Exception {
        RequestPolicyUSB requestPolicyUSB = new RequestPolicyUSB();

        requestPolicyUSB.setPType("1");
        requestPolicyUSB.setReqDeviceType("N");
        requestPolicyUSB.setReqMgmtKey("201707809123213");

        System.out.println(requestPolicyUSB.toXml());
    }

}