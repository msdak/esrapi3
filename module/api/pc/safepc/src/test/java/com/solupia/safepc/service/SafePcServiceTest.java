package com.solupia.safepc.service;

import com.solupia.safepc.dto.PcControlResult;
import com.solupia.safepc.dto.PcExecResult;
import com.solupia.safepc.dto.RequestApplyPolicy;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;

/**
 * Created by solupia on 2017. 4. 5..
 */
public class SafePcServiceTest {

    @Test
    public void getApplyPolicy() throws Exception {

        RequestApplyPolicy requestApplyPolicy = new RequestApplyPolicy();

        requestApplyPolicy.setReqNo("REQyyyyMMddHHmmssSSS");
        requestApplyPolicy.setUserID("testID");
        requestApplyPolicy.setAppPersonId("apprvId");
        requestApplyPolicy.setReqDT(LocalDateTime.now());
        requestApplyPolicy.setAppDT(LocalDateTime.of(2017, Month.JUNE, 5, 4, 0));
        requestApplyPolicy.setStartDT(LocalDateTime.of(2017, Month.MAY, 2, 23, 0));
        requestApplyPolicy.setEndDT(LocalDateTime.of(2017, Month.JULY, 3, 23, 0));
        requestApplyPolicy.setAppState(2);
        requestApplyPolicy.setReqReason("req test");
        requestApplyPolicy.setReqMedia(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setReqMediaRead(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setReqMediaBlock(Arrays.asList("fdd", "hdd"));
        requestApplyPolicy.setIpLock(1);

        System.out.println(requestApplyPolicy.toXml());

        SafePcService safePcService = new SafePcService();

        PcExecResult pcExecResult = safePcService.postApplyPolicy(requestApplyPolicy.toXml());

        System.out.println("response : " + pcExecResult.getCode());

    }


    @Test
    public void getUserRole() throws IOException {
        SafePcService safePcService = new SafePcService();
        String userRole = safePcService.getUserRole("201123");

        System.out.println(userRole);
    }
}