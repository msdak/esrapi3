package com.solupia.esr.web.api.kyobo.drm.dto;

import com.solupia.esr.web.api.kyobo.dlp.dto.DLPFileInfoDTO;
import lombok.Data;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DRMFileInfoDTO {

    private Long   fileId;
    private String dirPath;
    private String fileName;
    private String orgFileName;
    private String ext;

    public static DRMFileInfoDTO of(DLPFileInfoDTO dlpFileInfoDTO) {
        DRMFileInfoDTO drmFileInfoDTO = new DRMFileInfoDTO();
        drmFileInfoDTO.setFileId(dlpFileInfoDTO.getFileId());
        drmFileInfoDTO.setDirPath(dlpFileInfoDTO.getDirPath());
        drmFileInfoDTO.setExt(dlpFileInfoDTO.getExt());
        drmFileInfoDTO.setFileName(dlpFileInfoDTO.getFileName());
        drmFileInfoDTO.setOrgFileName(dlpFileInfoDTO.getOrgFileName());
        return drmFileInfoDTO;
    }
}
