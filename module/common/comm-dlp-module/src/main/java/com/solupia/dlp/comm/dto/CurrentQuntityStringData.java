package com.solupia.dlp.comm.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 2. 24..
 */
@Getter @Setter
public class CurrentQuntityStringData {
    private String name;
    private String count;

    public CurrentQuntityStringData() {
    }

    public CurrentQuntityStringData(String name, String count) {
        this.name = name;
        this.count = count;
    }
}
