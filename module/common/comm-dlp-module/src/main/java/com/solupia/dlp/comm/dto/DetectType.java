package com.solupia.dlp.comm.dto;

/**
 * Created by solupia on 2017. 2. 13..
 */
public enum DetectType {

    BE_DETECTED, // 검출됨
    NOT_DETECTABLE, // 검출되지 않음
    NON_DETECTABLE // 검출 불가능

}