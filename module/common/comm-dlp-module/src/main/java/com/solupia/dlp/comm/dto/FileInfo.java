package com.solupia.dlp.comm.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by solupia on 2017. 2. 7..
 */
@Getter @Setter
public class FileInfo {

    private DetectType privacyInfo;
    private Long fileId;
    private String dirPath;
    private String fileName;
    private String privacyContent;
    private String nonDetectReason;
    private MinimumQuantityData quantityData;

    private List<CurrentQuntityStringData> currentQuntityStringData;

}
