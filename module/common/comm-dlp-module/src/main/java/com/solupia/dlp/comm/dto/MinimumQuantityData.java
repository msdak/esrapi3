package com.solupia.dlp.comm.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 2. 7..
 */
@Getter @Setter
public class MinimumQuantityData {

    private Long resRegCode = -1L;
    private Long transferAccount = -1L;
    private Long creditCard = -1L;
    private Long email = -1L;
    private Long phone = -1L;
    private Long cellPhone = -1L;
    private Long normalFile = -1L;
    private Long detectImpossibility = -1L;

    public void setQuntityDataResult() {
        if(this.resRegCode == -1L) this.resRegCode = 0L;
        if(this.transferAccount == -1L) this.transferAccount = 0L;
        if(this.creditCard == -1L) this.creditCard = 0L;
        if(this.email == -1L) this.email = 0L;
        if(this.phone == -1L) this.phone = 0L;
        if(this.cellPhone == -1L) this.cellPhone = 0L;
        if(this.normalFile == -1L) this.normalFile = 0L;
        if(this.detectImpossibility == -1L) this.detectImpossibility = 0L;
    }
}
