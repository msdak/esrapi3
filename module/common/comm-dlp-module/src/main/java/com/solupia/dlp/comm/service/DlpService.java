package com.solupia.dlp.comm.service;


import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.dto.MinimumQuantityData;

import java.util.List;

/**
 * Created by solupia on 2017. 2. 7..
 */
public interface DlpService {
    public FileInfo parsingContent(FileInfo fileInfo, MinimumQuantityData minimumQuantityData, String charSet);
    boolean isDlpCompl(List<FileInfo> fileList);
    String getDefaultDLPPath();
}
