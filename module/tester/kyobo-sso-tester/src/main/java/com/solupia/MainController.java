package com.solupia;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by solupia on 2017. 4. 24..
 */
@Controller
public class MainController  {

    @RequestMapping("/sso/{fileName}")
    public String main(@PathVariable String fileName) throws IOException {
        System.out.println("index............");

        Files.copy(Paths.get("C:/" + fileName), Paths.get("C:/Temp/" + fileName));

        return "index";
    }

}
