<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="kbli.seed.service.*" %>
<%@ page import="java.util.Date" %>

<%
    Date now = new Date();
    long serverTime = now.getTime() / 1000; 		// 밀리세컨드에서 초 단위로 변경 (UNIX timestamp로 변경)
    String serverTimeString = new Long(serverTime).toString(); 	// UNIX timestamp String 획득
    String encryptedServerTimeString = null;
    String encryptedIDString = null;

    try {
        encryptedIDString = Encrypt.encryptEZTokenID("09929189", serverTimeString);
        encryptedServerTimeString = Encrypt.encryptEZTokenServerTime(serverTimeString);

    } catch (Exception e) {
        throw new Exception("seed encryption fail"); // Error 처리 (업무 시스템에 따라 처리가 달라짐)
    }
        
%>

<html>
<head>
<title>Localized Dates</title>
    <OBJECT ID="senx" WIDTH="10" HEIGHT="10" CLASSID="CLSID:7A8ACD16-D7F1-4887-868E-E4FFAB7962DA"
            CODEBASE="senx.cab#version=2,0,1,1">
    </OBJECT>
</head>
<body bgcolor="white">
<form name="ssoN2" action="http://localhost:8080/bridge" method="POST">
    <input type="hidden" name="eztoken">
    <input type="hidden" name="eztokentime">
</form>
<script>

    senx.SetTokenID('<%=encryptedIDString%>', '<%=encryptedServerTimeString%>'); // LOGIN_SUCCESS_TYPE

    var target = 'mis';
    var eztoken = senx.getEzmoreToken ("<%=encryptedIDString%>", "<%=encryptedServerTimeString%>");
    theWin = window.open("about:blank", target, "toolbar=yes,location=yes,menubar=yes,status=yes,directories=yes,scrollbars=yes,resizable=yes,width=1024,height=720,left=0,top=0");
    form = document.ssoN2;
    form.eztoken.value=eztoken;
    form.eztokentime = "<%=encryptedServerTimeString%>";
    form.target = target;
//    form.submit();
</script>


</body>
</html>