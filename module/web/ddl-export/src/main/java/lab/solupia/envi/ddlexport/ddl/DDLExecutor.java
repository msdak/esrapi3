package lab.solupia.envi.ddlexport.ddl;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lab.solupia.envi.ddlexport.dto.TevDbAccount;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public abstract class DDLExecutor {

    private static final String FILE_EXT = ".sql";
    protected String rootPath;
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    protected TevDbAccount tevDbAccount;

    abstract public void  executeDDL();

    public void initJdbcTemplate(TevDbAccount tevDbAccount) {
        this.tevDbAccount = tevDbAccount;

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(tevDbAccount.getJdbcDriver());
        hikariConfig.setJdbcUrl("jdbc:" + tevDbAccount.getJdbcUrl());
        hikariConfig.setUsername(tevDbAccount.getAccoId());
        hikariConfig.setPassword(tevDbAccount.getAccoPwd());

        DataSource dataSource = new HikariDataSource(hikariConfig);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate(dataSource));

    }

    public void makeFile(String folderPath , String path, String content) throws IOException {
        Path dir = Paths.get(rootPath + File.separator + folderPath + File.separator);
        if(!Files.exists(dir)){
            Files.createDirectory(dir);
        }
        Files.write(Paths.get(dir + path + FILE_EXT), content.getBytes(), StandardOpenOption.WRITE);
    }

    @Getter
    @AllArgsConstructor
    protected class ResultDTO{
        private String objectName;
        private String ddl;
    }
}
