package lab.solupia.envi.ddlexport.ddl.impl;

import lab.solupia.envi.ddlexport.ddl.DDLExecutor;
import lab.solupia.envi.ddlexport.dto.TevDbAccount;
import lab.solupia.envi.ddlexport.query.OracleQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.io.IOException;

@Slf4j
public class OracleDDLExecutor extends DDLExecutor {

    public OracleDDLExecutor(TevDbAccount tevDbAccount, String rootPath) {
        this.rootPath = rootPath;
        this.initJdbcTemplate(tevDbAccount);
    }

    @Override
    public void executeDDL() {
        this.runOracleMetaData("TABLE");
        this.runOracleMetaData("VIEW");
        this.runOracleMetaData("PROCEDURE");
        this.runOracleMetaData("PACKAGE");
        this.runOracleMetaData("MATERIALIZED_VIEW");
        this.runOracleMetaData("CONSTRAINT");
    }

    private void runOracleMetaData(String objectType){
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("owner",  this.tevDbAccount.getSchemaOwner())
                .addValue("objectType", objectType);

        namedParameterJdbcTemplate.query(OracleQuery.SELECT_TABLE, parameters, (rs, rowNum) ->
                new ResultDTO(rs.getString(1), rs.getString(2))).forEach(ddl -> {
            try {
                log.info(ddl.getDdl());
                this.makeFile( tevDbAccount.getSysId(), objectType + "_" + ddl.getObjectName(), ddl.getDdl());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
