package lab.solupia.envi.ddlexport.ddl.impl;

import lab.solupia.envi.ddlexport.ddl.DDLExecutor;
import lab.solupia.envi.ddlexport.dto.TevDbAccount;
import lab.solupia.envi.ddlexport.query.TiberoQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.io.IOException;

@Slf4j
public class TiberoDDLExecutor extends DDLExecutor {

    public TiberoDDLExecutor(TevDbAccount tevDbAccount, String rootPath) {
        this.rootPath = rootPath;
        this.initJdbcTemplate(tevDbAccount);
    }

    @Override
    public void executeDDL() {
        this.runTiberoMetaData("TABLE");
        this.runTiberoMetaData("VIEW");
        this.runTiberoMetaData("PROCEDURE");
        this.runTiberoMetaData("PACKAGE");
        this.runTiberoMetaData("MATERIALIZED_VIEW");
        this.runTiberoMetaData("CONSTRAINT");
    }

    private void runTiberoMetaData(String objectType){
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("owner",  this.tevDbAccount.getSchemaOwner())
                .addValue("objectType", objectType);

        namedParameterJdbcTemplate.query(TiberoQuery.SELECT_TABLE, parameters, (rs, rowNum) ->
                new ResultDTO(rs.getString(1), rs.getString(2))).forEach(ddl -> {
            try {
                log.info(ddl.getDdl());
                this.makeFile( tevDbAccount.getSysId(), objectType + "_" + ddl.getObjectName(), ddl.getDdl());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
