package lab.solupia.envi.ddlexport.dto;

public enum DbmsType {
    ORACLE,
    TIBERO
}
