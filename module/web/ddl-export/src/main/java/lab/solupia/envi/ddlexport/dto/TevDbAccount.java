package lab.solupia.envi.ddlexport.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TevDbAccount {
    private String sysId;
    private String sysNm;
    private String appId;
    private String dbmsTyp;
    private String jdbcDriver;
    private String jdbcUrl;
    private String accoId;
    private String accoPwd;
    private String schemaOwner;
}
