package lab.solupia.envi.ddlexport.service;

import lab.solupia.envi.ddlexport.EnviQury;
import lab.solupia.envi.ddlexport.ddl.DDLExecutor;
import lab.solupia.envi.ddlexport.dto.DbmsType;
import lab.solupia.envi.ddlexport.dto.TevDbAccount;
import lab.solupia.envi.ddlexport.ddl.impl.OracleDDLExecutor;
import lab.solupia.envi.ddlexport.ddl.impl.TiberoDDLExecutor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExportFactory {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Value("${envi.rootDir}")
    private String rootPath;

    @PostConstruct
    public void runExport(){
        List<TevDbAccount> tevDbAccounts = namedParameterJdbcTemplate.query(EnviQury.SELECT_DB_ACCO, (rs, rowNum) ->
            new TevDbAccount(
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                    rs.getString(4),
                    rs.getString(5),
                    rs.getString(6),
                    rs.getString(7),
                    rs.getString(8),
                    rs.getString(9)));

        tevDbAccounts.forEach(tevDbAccount -> {
            DDLExecutor ddlExecutor = initDDLExecutor(tevDbAccount);
            ddlExecutor.executeDDL();
        });

        log.info("end..............ddl");
    }


    private DDLExecutor initDDLExecutor(TevDbAccount tevDbAccount){
        if(rootPath.lastIndexOf(File.separator) != -1){
            rootPath = rootPath.substring(0, rootPath.lastIndexOf(File.separator));
        }
        DbmsType dbmsType = DbmsType.valueOf(tevDbAccount.getDbmsTyp());
        if(dbmsType == DbmsType.TIBERO){
            return new TiberoDDLExecutor(tevDbAccount, rootPath);
        }else{
            return new OracleDDLExecutor(tevDbAccount, rootPath);
        }
    }
}
