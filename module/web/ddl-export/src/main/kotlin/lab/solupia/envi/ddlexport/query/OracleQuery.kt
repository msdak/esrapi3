package lab.solupia.envi.ddlexport.query

class OracleQuery {
    companion object {
        @JvmField
        val SELECT_TABLE = """ 
                           SELECT OBJECT_NAME, dbms_metadata.GET_DDL(:objectType, OBJECT_NAME, :owner) AS DDL
                           FROM DBA_OBJECTS
                           where OWNER = :owner and OBJECT_TYPE = :objectType
                           """
    }
}