package lab.solupia.envi.ddlexport.query

class TiberoQuery {
    companion object {
        @JvmField
        val SELECT_TABLE = """ 
                           SELECT OBJECT_NAME, DBMS_METADATA.GET_DDL(:objectType, OBJECT_NAME, :owner)
                           FROM DBA_OBJECTS
                           where OWNER = :owner and OBJECT_TYPE = :objectType
                           """
    }
}