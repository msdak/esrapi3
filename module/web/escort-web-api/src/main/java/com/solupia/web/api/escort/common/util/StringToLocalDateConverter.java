package com.solupia.web.api.escort.common.util;

import javax.persistence.AttributeConverter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class StringToLocalDateConverter implements AttributeConverter<LocalDate, String> {

    @Override
    public LocalDate convertToEntityAttribute(String source) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return (Objects.isNull(source) || source.isEmpty()) ? null : LocalDate.parse(source, formatter);
    }

    @Override
    public String convertToDatabaseColumn(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return localDate == null ? null : localDate.format(formatter);
    }
}
