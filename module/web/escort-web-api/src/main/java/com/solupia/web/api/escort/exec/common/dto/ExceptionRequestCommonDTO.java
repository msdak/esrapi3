package com.solupia.web.api.escort.exec.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
public class ExceptionRequestCommonDTO {

    private String appId;           //에스코트 서버에 부여된 esrAPI ID
    private String serial;          //징치 Serial 번호
    private LocalDateTime fromDate; //예외 신청 시 예외 시작 일
    private LocalDateTime toDate;   //예외 신청 시 예외 종료 일
    private String desc;            //예외 신청 시 사유
    private String empNo;           //예외 신청 시 예외 신청자 사번

    private boolean isSync;         //예외 신청 시 대상 장치에 정책 바로 동기화 여부
}
