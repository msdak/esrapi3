package com.solupia.web.api.escort.exec.device.controller;


import com.solupia.web.api.escort.exec.device.dto.DeviceExceptionDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DeviceRestController {

    /**
     *
     * @param deviceExceptionDTO
     */
    @PutMapping
    public void deviceException(@RequestBody DeviceExceptionDTO deviceExceptionDTO) {

    }

}
