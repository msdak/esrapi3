package com.solupia.web.api.escort.exec.device.dto;

import com.google.common.collect.ImmutableMap;
import com.solupia.web.api.escort.exec.common.dto.ExceptionRequestCommonDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter @Setter
public class DeviceExceptionDTO extends ExceptionRequestCommonDTO {

    private static final Map<String, String> DEVICE_MAP = ImmutableMap.<String, String>builder()
            .put("WEBCAM", "C" )
            .put("USBMODEM", "U")
            .put("WIBRO" , "R")
            .put("PCMCIA", "P")
            .put("IFEE1394", "9")
            .put("REDPOINTER", "D")
            .put("SCANNER", "A")
            .put("BOOTPWD", "B")
            .put("FOLDERSHARE", "F")
            .put("ANTIVIRUS", "V")
            .put("SCREENSAVOR", "S")
            .put("WIRELESS", "W")
            .put("SMARTPHONE", "M")
            .put("BLUETOOTH", "T")
            .build();

    private String device;

    public void setDevice(String device) {
        this.device = DEVICE_MAP.get(device.toUpperCase());
    }
}
