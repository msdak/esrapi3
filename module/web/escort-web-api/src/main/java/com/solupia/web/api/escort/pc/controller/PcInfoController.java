package com.solupia.web.api.escort.pc.controller;

import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.service.PcInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PcInfoController {

    private final PcInfoService pcInfoService;

    @GetMapping("/pc")
    public Page<IpBasicDTO> getPcList(
            PcInfoSearchDTO pcInfoSearchDTO,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sort", defaultValue = "ASC") Sort.Direction sort) {
        return pcInfoService.getPcList(pcInfoSearchDTO, new PageRequest(page, size, new Sort(sort, "insaInfo.empno")));
    }



}
