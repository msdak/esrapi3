package com.solupia.web.api.escort.pc.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class IpBasicDTO {

    private String empno;
    private String hname;
    private String ipAddr;
}
