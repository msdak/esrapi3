package com.solupia.web.api.escort.pc.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.PageRequest;

@Setter @Getter
public class PcInfoSearchDTO {


    private String hname;
    private String empno;
    private String ipAddr;

}
