package com.solupia.web.api.escort.pc.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "INSAINFO")
@Getter @Setter
public class InsaInfo {

    @Id
    private String empno;

    private String hname;
    private String resno;
    private String sdeptnm;
    private String locate1;
    private String locate2;
    private String status;
    private String enterdate;
    private String outdate;
    private String emptype;
    private String indeptnm;
    private String indept;
    private String jikgubnm;
    private String jikgan;
    private String locatecode;
    private String password;
    private String userKey;
    private String useStartdate;
    private String useEnddate;
    private Long useEndUser;
    private Long useEndIp;
    private Long useEndPc;
    private Long onbusiness;
    private String hhpNo;
    private String origin;
    private String uid;
    private String oldEmpno;
    private String sdeptcode;
    private String company;
    private String deptcode;
    private String publicReg;
    private String empkind;
    private Long gradelevel;
    private String ejikgubnm;
    private String language;
    private String madecode;
    private String companyname;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "insaInfo")
    private List<IpBasic> ipBasicList;
}
