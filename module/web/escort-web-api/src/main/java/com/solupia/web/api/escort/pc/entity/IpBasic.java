package com.solupia.web.api.escort.pc.entity;

import com.solupia.web.api.escort.common.util.StringToLocalDateConverter;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter @Setter
public class IpBasic {

    @Id
    private String ipAddr;
    private String macAddr;
    private String ipNet;
    private String hname;
    private String sdeptnm;
    private String realUser;
    private String clientNm;
    private String clientSn;
    @Convert(converter = StringToLocalDateConverter.class)
    private LocalDate regDate;
    @Convert(converter = StringToLocalDateConverter.class)
    private LocalDate usedDate;
    private String regPrsn;
    private String management;
    private String status;
    private String dhcp;
    private Long publicYn;
    private String deptcode;
    private String locatenm;
    private String madecode;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "empno")
    private InsaInfo insaInfo;
}
