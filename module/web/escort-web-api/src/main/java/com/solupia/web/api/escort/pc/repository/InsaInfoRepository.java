package com.solupia.web.api.escort.pc.repository;


import com.solupia.web.api.escort.pc.entity.InsaInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsaInfoRepository extends JpaRepository<InsaInfo, String>{

}
