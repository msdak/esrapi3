package com.solupia.web.api.escort.pc.repository;

import com.solupia.web.api.escort.pc.entity.IpBasic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface IpBasicRepository extends JpaRepository<IpBasic, String> , IpBasicRepositoryCustom {

}
