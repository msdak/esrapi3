package com.solupia.web.api.escort.pc.repository;

import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IpBasicRepositoryCustom {
    Page<IpBasicDTO> search(PcInfoSearchDTO pcInfoSearchDTO, Pageable pageable);
}
