package com.solupia.web.api.escort.pc.repository.custom.impl;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.JPQLQuery;
import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import com.solupia.web.api.escort.pc.entity.IpBasic;
import com.solupia.web.api.escort.pc.entity.QIpBasic;
import com.solupia.web.api.escort.pc.repository.IpBasicRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@Repository
public class IpBasicRepositoryImpl extends QueryDslRepositorySupport implements IpBasicRepositoryCustom {

    public IpBasicRepositoryImpl(EntityManager entityManager) {
        super(IpBasic.class);
        super.setEntityManager(entityManager);
    }

    @Override
    public Page<IpBasicDTO> search(PcInfoSearchDTO pcInfoSearchDTO, Pageable pageable) {
        QIpBasic ipBasic = QIpBasic.ipBasic;
        JPQLQuery<IpBasic> query = from(ipBasic);

        if(Objects.nonNull(pcInfoSearchDTO.getIpAddr())){
            query.where(ipBasic.ipAddr.like("%" + pcInfoSearchDTO.getIpAddr() + "%"));
        }

        if(Objects.nonNull(pcInfoSearchDTO.getEmpno())){
            query.where(ipBasic.insaInfo.empno.like("%" + pcInfoSearchDTO.getEmpno() + "%"));
        }

        if(Objects.nonNull(pcInfoSearchDTO.getHname())){
            query.where(ipBasic.hname.like("%" + pcInfoSearchDTO.getHname() + "%"));
        }

        List<IpBasicDTO> list = getQuerydsl().applyPagination(pageable,
                query.select(
                            Projections.constructor(IpBasicDTO.class, ipBasic.insaInfo.empno, ipBasic.insaInfo.hname, ipBasic.ipAddr))
                    .innerJoin(ipBasic.insaInfo))
                .fetch();

        long totalCount = query.fetchCount();

        return new PageImpl<>(list, pageable, totalCount);
    }
}
