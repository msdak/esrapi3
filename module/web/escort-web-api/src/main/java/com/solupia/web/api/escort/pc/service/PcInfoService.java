package com.solupia.web.api.escort.pc.service;

import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PcInfoService {

    Page<IpBasicDTO> getPcList(PcInfoSearchDTO pcInfoSearchDTO, Pageable pageable);
}
