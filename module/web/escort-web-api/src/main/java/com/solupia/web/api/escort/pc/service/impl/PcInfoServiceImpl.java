package com.solupia.web.api.escort.pc.service.impl;

import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import com.solupia.web.api.escort.pc.repository.IpBasicRepository;
import com.solupia.web.api.escort.pc.service.PcInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class PcInfoServiceImpl implements PcInfoService {

    private final IpBasicRepository ipBasicRepository;

    @Override
    @Transactional
    public Page<IpBasicDTO> getPcList(PcInfoSearchDTO pcInfoSearchDTO, Pageable pageable) {
        return ipBasicRepository.search(pcInfoSearchDTO, pageable);
    }
}
