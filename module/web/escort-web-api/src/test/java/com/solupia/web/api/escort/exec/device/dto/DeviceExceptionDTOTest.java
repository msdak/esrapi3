package com.solupia.web.api.escort.exec.device.dto;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class DeviceExceptionDTOTest {

    @Test
    public void setDevice() {
        DeviceExceptionDTO deviceExceptionDTO = new DeviceExceptionDTO();

        deviceExceptionDTO.setDevice("webCam");

        assertThat("C", is(deviceExceptionDTO.getDevice()));
    }
}