package com.solupia.web.api.escort.pc.repository;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.solupia.web.api.escort.pc.entity.InsaInfo;
import com.solupia.web.api.escort.pc.entity.QInsaInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class InsaInfoRepositoryTest {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private InsaInfoRepository insaInfoRepository;


    @Test
    public void test_91() {
//        List<InsaInfo> collect = insaInfoRepository.findAll().stream().limit(3L).collect(toList());
//
//        assertThat(3, is(collect.size()));
//
//        List<IpBasic> ipBasicList = collect.get(0).getIpBasicList();
//
//
//        System.out.println(ipBasicList);

        JPAQueryFactory query = new JPAQueryFactory(entityManager);

        QInsaInfo qInsaInfo = QInsaInfo.insaInfo;
        List<InsaInfo> fetch = query
                .selectFrom(qInsaInfo)
                .where(qInsaInfo.ipBasicList.size().gt(0))
                .fetch();
        System.out.println(fetch);

    }
}