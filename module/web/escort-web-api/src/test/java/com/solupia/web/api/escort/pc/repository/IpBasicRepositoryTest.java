package com.solupia.web.api.escort.pc.repository;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import com.solupia.web.api.escort.pc.entity.IpBasic;
import com.solupia.web.api.escort.pc.entity.QInsaInfo;
import com.solupia.web.api.escort.pc.entity.QIpBasic;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.awt.print.Pageable;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class IpBasicRepositoryTest {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    private IpBasicRepository ipBasicRepository;

    @Test
    public void finde_test() {
        List<IpBasic> collect = ipBasicRepository.findAll().stream()
                .filter(v -> Objects.nonNull(v.getInsaInfo()))
                .collect(toList());
        System.out.println(collect.size());
//        collect.get(0).getInsaInfo();
//        assertThat(3, is(collect.size()));
    }

    @Test
    public void join_test() {
        JPAQuery query = new JPAQuery(entityManager);
        QIpBasic ipBasic = QIpBasic.ipBasic;
        List fetch = query.select(ipBasic.ipAddr, ipBasic.insaInfo.hname).from(ipBasic)
                .innerJoin(ipBasic.insaInfo)
                .fetch();
        System.out.println(fetch);

        JPAQueryFactory query2 = new JPAQueryFactory(entityManager);

        List<IpBasicDTO> fetch1 =
                query2.select(
                        Projections.constructor(
                                IpBasicDTO.class, ipBasic.insaInfo.empno, ipBasic.insaInfo.hname, ipBasic.ipAddr))
                .from(ipBasic)
                .innerJoin(ipBasic.insaInfo)
                .offset(10L)
                .limit(10L)
                .fetch();

        System.out.println(fetch1);

    }
}