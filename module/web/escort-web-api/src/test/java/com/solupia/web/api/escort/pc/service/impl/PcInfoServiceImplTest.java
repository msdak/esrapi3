package com.solupia.web.api.escort.pc.service.impl;

import com.solupia.web.api.escort.pc.dto.PcInfoSearchDTO;
import com.solupia.web.api.escort.pc.dto.IpBasicDTO;
import com.solupia.web.api.escort.pc.service.PcInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PcInfoServiceImplTest {

    @Autowired
    private PcInfoService pcInfoService;

    @Test
    public void getPcList() throws Exception {
        PcInfoSearchDTO pcInfoSearchDTO = new PcInfoSearchDTO();
        Page<IpBasicDTO> pcList = pcInfoService.getPcList(pcInfoSearchDTO, new PageRequest(0, 10));

        System.out.println(pcList);
    }

}