package com.solupia.esr.web.api.kyobo;

import com.solupia.dlp.comm.service.DlpService;
import com.solupia.markany.config.MarkAnyConfig;
import com.solupia.markany.service.MarkAnyDBService;
import com.solupia.safepc.config.SafePcConfig;
import com.solupia.safepc.service.SafePcService;
import com.solupia.somansa.config.SomansaDLPConfig;
import com.solupia.somansa.service.DlpServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Created by solupia on 2017. 4. 6..
 */
@SpringBootApplication
public class KyoboWebApplication {

    public static final String[] SUPOT_DRM_EXTS = {".ppt", ".pptx", ".doc", ".docx", ".xls", ".xlsx", ".rtf",
            ".bmp", ".dib", ".gif", ".jpg", ".jpeg", ".tif", ".tiff", ".png", ".jpe", ".jfif",
            ".hwp", ".pdf", ".txt", ".csv"};

    @Bean
    public DlpService setDlpService() {
        return new DlpServiceImpl();
    }

    @Bean
    public SafePcService setSafePcService() {
        return new SafePcService();
    }

    @Bean
    public MarkAnyDBService setMarkAnyDBService() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        dataSource.setDriverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

        dataSource.setUrl("jdbc:sqlserver://10.33.5.213:1433;databaseName=DRM");
        dataSource.setUsername("markany");
        dataSource.setPassword("kyobo65!@");

//        dataSource.setUrl("jdbc:sqlserver://123.212.44.78:19010;databaseName=KYOBO_DEV");
//        dataSource.setUrl("jdbc:sqlserver://192.168.1.223:1401;databaseName=KYOBO_DEV");
//        dataSource.setUsername("kyobo_dev1");
//        dataSource.setPassword("dev123!@#");

        jdbcTemplate.setDataSource(dataSource);
        return new MarkAnyDBService(jdbcTemplate);
    }

    public static void main(String[] args) {
        SafePcConfig.REQUEST_URL = "http://192.168.1.158:9090";

        MarkAnyConfig.ENCFILE_DIR      = "C:/eSpider/files/file/download/";
        MarkAnyConfig.DRMFILE_DIR      = "C:/eSpider/files/file/upload/";


        SomansaDLPConfig.SOMANSA_DIR_PAHT = "C:/DLP/";


//        SafePcConfig.REQUEST_URL = "http://192.168.1.158:9090";

//        MarkAnyConfig.ENCFILE_DIR      = "/Volumes/D/eSpider/files//file/download/";
//        MarkAnyConfig.DRMFILE_DIR = "/Volumes/D/eSpider/files//file/upload/";

//        MarkAnyConfig.ENCFILE_DIR      = "/Volumes/eSpider/files/file/download/";
//        MarkAnyConfig.DRMFILE_DIR      = "/Volumes/eSpider/files/file/upload/";

//        MarkAnyConfig.ENCFILE_DIR      = "/Users/solupia/Documents/test/files/file/download/";
//        MarkAnyConfig.DRMFILE_DIR      = "/Users/solupia/Documents/test/files/file/upload/";

//        SomansaDLPConfig.SOMANSA_DIR_PAHT = "/Users/meongsoojang/Projects/Test/files/DLP/";
//        SomansaDLPConfig.SOMANSA_DIR_PAHT = "/Volumes/eSpider/DLP/";


        SpringApplication.run(KyoboWebApplication.class, args);
    }
}
