package com.solupia.esr.web.api.kyobo.common;

import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class ResultServiceReq {
    private Long svcId;
    private String message;
    private String code;
    private String chnCode;
    private List<?> result;

    public ResultServiceReq(Long svcId, String message, String code, String chnCode) {
        this.svcId = svcId;
        this.message = message;
        this.code = code;
        this.chnCode = chnCode;
    }

    public ResultServiceReq(Long svcId, String message, String code) {
        this.svcId = svcId;
        this.message = message;
        this.code = code;
    }
}
