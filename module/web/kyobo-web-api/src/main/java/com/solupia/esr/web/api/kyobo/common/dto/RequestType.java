package com.solupia.esr.web.api.kyobo.common.dto;

/**
 * @author jason, Moon.
 * @since 2017-04-05.
 */
public enum RequestType {
    READ, WRITE, NONE
}
