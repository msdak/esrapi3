package com.solupia.esr.web.api.kyobo.common.dto;

/**
 * @author jason, Moon
 * @since 2016. 11. 8.
 */
public enum YesOrNo {
    YES,
    NO;

    public static YesOrNo of(boolean trueOrFalse) {
        return trueOrFalse ? YesOrNo.YES : YesOrNo.NO;
    }
}
