package com.solupia.esr.web.api.kyobo.common.refrence.common;

/**
 * Created by solupia on 2017. 4. 7..
 */
public class CodeConstant {

    public static final String SUCCESS_CODE = "200";
    public static final String FAIL_CODE = "500";

}
