package com.solupia.esr.web.api.kyobo.common.refrence.common;

/**
 * Created by solupia on 2017. 4. 7..
 */
public class MsgConstant {

    public static final String SUCCESS_MSG = "성공";
    public static final String FAIL_MSG = "실패";
    public static final String FILE_LIST_IS_EMPTY = "파일 목록이";
    public static final String UNKNOWN_EXCEPTION_MSG = "알 수 없는 오류입니다.";

}
