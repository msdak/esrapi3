package com.solupia.esr.web.api.kyobo.common.refrence.common;

import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 4. 14..
 */
public class TimeFormat {
    public static final DateTimeFormatter dtfYYYMMDD = DateTimeFormatter.ofPattern("yyyyMMdd");
}
