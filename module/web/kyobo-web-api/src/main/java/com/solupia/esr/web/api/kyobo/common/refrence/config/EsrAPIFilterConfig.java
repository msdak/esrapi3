package com.solupia.esr.web.api.kyobo.common.refrence.config;

import com.solupia.esr.web.api.kyobo.common.refrence.filter.APIKeyFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by solupia on 2017. 4. 6..
 */
@Configuration
public class EsrAPIFilterConfig  {

    @Bean
    public FilterRegistrationBean getFilterRegistrationBean(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new APIKeyFilter());
        registrationBean.addUrlPatterns("/*"); // 서블릿 등록 빈 처럼 패턴을 지정해 줄 수 있다.
        return registrationBean;
    }

}
