package com.solupia.esr.web.api.kyobo.common.refrence.config;


/**
 * EsrApi의 실행설정 옵션들은 
 * EsrApiConfig의 Spring Bean 값으로 설정해서 사용하자. 
 *  (Properties는 너무 지저분하다...)
 */
public class EsrApiConfig {

  private String serverId = "";

  public String getServerId() {
    return serverId;
  }

  public void setServerId(String serverId) {
    this.serverId = serverId;
  }
  
}
