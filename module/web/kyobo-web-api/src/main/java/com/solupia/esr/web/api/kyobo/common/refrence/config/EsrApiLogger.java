package com.solupia.esr.web.api.kyobo.common.refrence.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;


public class EsrApiLogger {
 
  private static Logger logger = LoggerFactory.getLogger( EsrApiLogger.class );

  public static void error( String msg ){ logger.error( msg ); }
  public static void error( String msg, HttpServletRequest req ){
    String allMsg = msg 
          + ( req==null ? "" : "\r\n uri:"+req.getRequestURI() )
        ;
    error( allMsg );
  }
  
  public static void warn( String msg ){ logger.warn( msg ); }
  public static void warn( String msg, HttpServletRequest req ){
    String allMsg = msg 
          + ( req==null ? "" : "\r\n uri:"+req.getRequestURI() )
        ;
    warn( allMsg );
  }


  public static void info( String msg ) { logger.info( msg );  }
  public static void info( String msg, HttpServletRequest req ){
    String allMsg = msg 
          + ( req==null ? "" : "\r\n uri:"+req.getRequestURI() )
        ;
    info( allMsg );
  }


  public static void debug( String msg ){ logger.debug( msg ); }
  public static void debug( String msg, HttpServletRequest req ){
    String allMsg = msg 
          + ( req==null ? "" : "\r\n uri:"+req.getRequestURI() )
        ;
    debug( allMsg );
  }

  
  public static void trace( String msg ){ logger.trace( msg ); }
  public static void trace( String msg, HttpServletRequest req ){
    String allMsg = msg 
          + ( req==null ? "" : "\r\n uri:"+req.getRequestURI() )
        ;
    trace( allMsg );
  }

  
  public static void exception( Exception ex ){    exception( ex, null );  }
  public static void exception( Exception ex, HttpServletRequest req  ){
    
    StringBuilder sb = new StringBuilder(1024);
    sb.append(" Exception - "+ex.getMessage() );
    if( req!=null ){
      sb.append( "\r\n uri:"+req.getRequestURI() );
    }
    StackTraceElement[] ste = ex.getStackTrace(); 
    for( int idx=0; idx<ste.length; idx++ ){
      sb.append("\r\n    "+ste[idx].getClassName()+" "+ste[idx].getMethodName()+" "+ste[idx].getLineNumber() );
    }
    logger.error( sb.toString() ); 
  }
  
}