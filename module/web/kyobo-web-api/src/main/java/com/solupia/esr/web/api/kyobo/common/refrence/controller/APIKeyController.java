package com.solupia.esr.web.api.kyobo.common.refrence.controller;

import com.solupia.esr.rsa.server.Util4IFFServer;
import com.solupia.esr.web.api.kyobo.common.refrence.config.EsrApiLogger;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.APIKeyDTO;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 4. 6..
 */
@RestController
public class APIKeyController {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    /**
     *
     * API 와 통신하기 위한 KEY 발급
     * @param initKey
     * @param clientId
     * @return
     */
    @PostMapping(value = "/getApikey")
    public ResultToObject<APIKeyDTO> getApikey(
            @RequestParam(name="initKey") String initKey, @RequestParam(name = "clientId") String clientId) {


        ResultToObject resultToObject = new ResultToObject();
        APIKeyDTO apiKeyDTO = new APIKeyDTO();
        String serverId = "EsrOp";

        EsrApiLogger.trace("/getApikey initkey:"+initKey+"  clientId:"+clientId+ "serverId:" + serverId);
        try{
            Util4IFFServer.init();
            String msg = Util4IFFServer.decode(clientId, initKey);

            if( !clientId.equals(msg) ){
                throw new Exception("클라이언트 인증에 실패했습니다.");
            }
            LocalDateTime curDate = LocalDateTime.now().plusHours(6L);
            String dateStr = dateTimeFormatter.format(curDate);

            // 클라이언트에게 제공할 인증키는, 클라이언트가 요청시 서버에게 제출하고 서버에서 인증키를 확인해야 하므로
            // 서버에서 디코딩하기 위해 서버 ID로 인코딩 함.
            String apiKey = Util4IFFServer.encode(serverId, clientId+";"+dateStr);

            apiKeyDTO.setCode("200");
            apiKeyDTO.setMsg("정상종료");
            apiKeyDTO.setApiKey(apiKey);
        } catch( Exception ex ){
            apiKeyDTO.setCode("500");
            apiKeyDTO.setMsg(ex.getMessage());
            EsrApiLogger.error("/getApikey Error:"+ex.getMessage() + "\n" + "initkey:" + initKey + "  clientId:" + clientId);
        }
        resultToObject.setResult(apiKeyDTO);
        return resultToObject;
    }
}
