package com.solupia.esr.web.api.kyobo.common.refrence.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 4. 6..
 *
 * {"status":"1","message":"정상실행","apikey":"QWER"}
 */
@Getter @Setter
public class APIKeyDTO {

    private String code;
    private String msg;
    private String apiKey;
    private String initKey;
    private String clientId;

}
