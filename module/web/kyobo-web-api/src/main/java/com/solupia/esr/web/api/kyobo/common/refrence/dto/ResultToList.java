package com.solupia.esr.web.api.kyobo.common.refrence.dto;

import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class ResultToList {
    private String status = CodeConstant.SUCCESS_CODE;
    private String message = MsgConstant.SUCCESS_MSG;
    private List<?> result;
}
