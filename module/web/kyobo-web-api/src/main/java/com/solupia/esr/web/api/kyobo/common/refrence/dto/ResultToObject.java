package com.solupia.esr.web.api.kyobo.common.refrence.dto;

import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import lombok.Data;

/**
 * Created by meongsoojang on 2017. 4. 9..
 */
@Data
public class ResultToObject<T> {
    private String status = CodeConstant.SUCCESS_CODE;
    private String message = MsgConstant.SUCCESS_MSG;
    private T result;
}
