package com.solupia.esr.web.api.kyobo.common.refrence.filter;

import com.solupia.esr.rsa.exception.ApikeyNotException;
import com.solupia.esr.rsa.exception.GeneralBizException;
import com.solupia.esr.rsa.server.Util4IFFServer;
import com.solupia.esr.web.api.kyobo.common.refrence.config.EsrApiLogger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by solupia on 2017. 4. 6..
 */
@WebFilter(urlPatterns = {"/*"}, description = "apikey Checker Filter")
public class APIKeyFilter implements Filter {

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    private boolean checkApikey(String apikey)  {
        String serverId = "EsrOp";
        String msg;
        try{
            EsrApiLogger.debug("APIKEY:"+apikey+"(serverId:"+serverId+")");
            Util4IFFServer.init();
            msg = Util4IFFServer.decode(serverId, apikey);
        }catch (Exception e) {
            throw new ApikeyNotException();
        }
        EsrApiLogger.debug("APIKEY message :: " + msg);

        if (msg == null || "".equals(msg)) {
            return false;
        }

        // message : clientId;YYYY-MM-DD HH24:mi:ss
        String[] token = msg.split(";");

        LocalDateTime curDate = LocalDateTime.now();

        String dateStr = dateTimeFormatter.format(curDate);

        if (0 < dateStr.compareTo(token[1])) {
            EsrApiLogger.error("APIKEY 거부됨 :: 기간만료. "+token[1] );
            //키가 만료되었음
            //한번 발급된 APIKEY는 6시간 동안 적용
            return false;
        }
        return true;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        long startTime = System.currentTimeMillis();
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        EsrApiLogger.warn("Request URL::" + request.getRequestURL().toString()  + ":: Start WkTime=" + System.currentTimeMillis());
        request.setAttribute("startTime", startTime);
        String reqUrl = request.getRequestURL().toString();
        if (reqUrl.matches(".+/getApikey(/|$)")) {
            String clientId = request.getParameter("clientId");
            if (clientId == null || "".equals(clientId)) {
                throw new GeneralBizException("client ID가 없습니다.");
            }
            String initkey = request.getParameter("initKey");
            if (initkey == null || "".equals(initkey)) {
                throw new GeneralBizException("initkey가 없습니다.");
            }
        } else {
            // 인증키가 맞는 지 확인
            String apikey = (String)request.getParameter("apikey").replaceAll(" ", "+");
            if (!this.checkApikey(apikey)) {
                throw new ApikeyNotException();
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
