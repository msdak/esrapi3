package com.solupia.esr.web.api.kyobo.common.util;

import com.solupia.esr.web.api.kyobo.KyoboWebApplication;

import java.util.Arrays;

public class DrmUtil {

    public static boolean isSupExt(String ext) {
        return Arrays.asList(KyoboWebApplication.SUPOT_DRM_EXTS).contains(ext.toLowerCase());
    }
}
