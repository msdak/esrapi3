package com.solupia.esr.web.api.kyobo.config;

import com.solupia.esr.web.api.kyobo.KyoboWebApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

/**
 * @author jason, Moon
 * @since 2016. 11. 17.
 */
@Configuration
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(KyoboWebApplication.class);
    }
}
