package com.solupia.esr.web.api.kyobo.dlp.controller;

import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.dlp.dto.DLPRequestDTO;
import com.solupia.esr.web.api.kyobo.dlp.service.DlpRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by solupia on 2017. 4. 7..
 */
@RestController
public class DlpWebController {

    @Autowired
    private DlpRequestService dlpRequestService;

    @PostMapping(value = "/dlp/detectPrivacyInfo", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList detectPrivacyInfo(@RequestBody DLPRequestDTO dlpRequestDTO) {
        return dlpRequestService.detectPrivacyInfo(dlpRequestDTO);
    }
}
