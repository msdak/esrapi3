package com.solupia.esr.web.api.kyobo.dlp.dto;

import lombok.Data;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DLPFileInfoDTO {

    private Long   fileId;
    private String dirPath;
    private String fileName;
    private String orgFileName;
    private String ext;
}
