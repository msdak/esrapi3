package com.solupia.esr.web.api.kyobo.dlp.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DLPRequestDTO {
    private String charSet;
    private List<DLPServiceDTO> dlpReqDTOS;
}
