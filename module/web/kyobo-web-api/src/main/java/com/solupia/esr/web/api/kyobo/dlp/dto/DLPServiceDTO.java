package com.solupia.esr.web.api.kyobo.dlp.dto;

import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.dto.MinimumQuantityData;
import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DLPServiceDTO {

    private Long svcId;
    private List<FileInfo> fileNameList;
    private MinimumQuantityData quantityData;
}
