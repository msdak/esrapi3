package com.solupia.esr.web.api.kyobo.dlp.service;

import com.solupia.dlp.comm.dto.DetectType;
import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.service.DlpService;
import com.solupia.esr.web.api.kyobo.common.ResultServiceReq;
import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.dlp.dto.DLPRequestDTO;
import com.solupia.esr.web.api.kyobo.dlp.dto.DLPServiceDTO;
import com.solupia.esr.web.api.kyobo.drm.dto.DRMFileInfoDTO;
import com.solupia.esr.web.api.kyobo.drm.service.DRMRequestService;
import com.solupia.markany.config.MarkAnyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by solupia on 2017. 4. 11..
 */
@Service
public class DlpRequestService {

    @Autowired
    private DlpService dlpService;

    @Autowired
    private DRMRequestService drmRequestService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DlpRequestService.class);

    public ResultToList detectPrivacyInfo(DLPRequestDTO dlpRequestDTO) {
        String defaultDLPPath = dlpService.getDefaultDLPPath();
        ResultToList resultToList = new ResultToList();
        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        Set<String> dirSet = new HashSet<>();
        for(DLPServiceDTO dlpServiceDTO : dlpRequestDTO.getDlpReqDTOS()) {
            ResultServiceReq resultServiceReq
                    = new ResultServiceReq(dlpServiceDTO.getSvcId(), MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE);
            List<FileInfo> fileInfos = new ArrayList<>();
            if(dlpServiceDTO.getFileNameList().size() == 0 ) {
                resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
            }
            for(FileInfo fileInfo: dlpServiceDTO.getFileNameList()){
                String targetDirPath = defaultDLPPath + File.separator + fileInfo.getDirPath();
                if(!Files.exists(Paths.get(targetDirPath))) {
                    new File(targetDirPath).mkdir();
                }
                //원본 파일 -> DRM 복호화
                int drmResult = drmRequestService.drmDocExternDec(DRMFileInfoDTO.of(fileInfo), targetDirPath + File.separator + fileInfo.getFileName());
//                int drmResult = 0;
                LOGGER.info("DLP DRM RESULT {}, {}", fileInfo.getFileName(), drmResult);
                /* 암호호 성공 or 암호화 파일 아님 */
                if(drmResult == 0) {
                    fileInfo = dlpService.parsingContent(fileInfo, dlpServiceDTO.getQuantityData(), dlpRequestDTO.getCharSet());
                }else if(drmResult == 52315) {
                    //원본파일 DLP 검사 폴더로 복사
                    try {
                        Files.copy(Paths.get(MarkAnyConfig.DRMFILE_DIR + fileInfo.getDirPath() + File.separator + fileInfo.getFileName()),
                                Paths.get(targetDirPath + File.separator + fileInfo.getFileName()) , StandardCopyOption.REPLACE_EXISTING) ;
                    } catch (IOException e) {
                        LOGGER.error("File copy error {}, tar {}",
                                MarkAnyConfig.DRMFILE_DIR + fileInfo.getDirPath() + File.separator + fileInfo.getFileName() ,
                                targetDirPath + File.separator + fileInfo.getFileName());
                    }
                    fileInfo = dlpService.parsingContent(fileInfo, dlpServiceDTO.getQuantityData(), dlpRequestDTO.getCharSet());
                }else{
                    fileInfo.setPrivacyInfo(DetectType.NON_DETECTABLE);
                }
                fileInfos.add(fileInfo);
                dirSet.add(targetDirPath);
            }
            resultServiceReq.setResult(fileInfos);
            resultServiceReqs.add(resultServiceReq);
        }
        resultToList.setResult(resultServiceReqs);
//        try {
//            for(String dirPath : dirSet) {
//                if(Files.exists(Paths.get(dirPath))) {
//                    Files.walk(Paths.get(dirPath))
//                        .map(Path::toFile)
//                        .forEach(file -> {
//                            LOGGER.info("FILE PATH : {}" , file.getAbsoluteFile() );
//                            file.delete();
//                        });
//                    if(new File(dirPath).exists()) new File(dirPath).delete();
//                }
//            }
//        } catch (IOException e) {
//            LOGGER.error("DLP File delete Exception " , e.getCause());
//        }
        return resultToList;
    }
}
