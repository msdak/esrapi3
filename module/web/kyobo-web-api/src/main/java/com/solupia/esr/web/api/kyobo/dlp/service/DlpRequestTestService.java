package com.solupia.esr.web.api.kyobo.dlp.service;

import com.solupia.dlp.comm.dto.DetectType;
import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.dlp.comm.dto.MinimumQuantityData;
import com.solupia.esr.web.api.kyobo.common.ResultServiceReq;
import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.dlp.dto.DLPRequestDTO;
import com.solupia.esr.web.api.kyobo.dlp.dto.DLPServiceDTO;
import com.solupia.markany.config.MarkAnyConfig;
import com.solupia.somansa.config.MinimumQuantityCode;
import com.solupia.somansa.config.SomansaDLPConfig;
import com.solupia.somansa.config.SomansaMinimumQuantityData;
import com.solupia.somansa.dto.Pattern;
import com.solupia.somansa.dto.PatternList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by solupia on 2017. 4. 11..
 */
@Service
public class DlpRequestTestService {


    private static final Logger LOGGER = LoggerFactory.getLogger(DlpRequestTestService.class);

    public PatternList parsingXML(String source) {
        PatternList patternList = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PatternList.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            patternList = (PatternList)unmarshaller.unmarshal(new StringReader(source));
            LOGGER.info("DLP XML {}", source);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return patternList;
    }

    public FileInfo parsingContent(FileInfo fileInfo, MinimumQuantityData minimumQuantityData, String charSet) {

        String source = "<?xml version='1.0' encoding='UTF-8'?><PatternList><Pattern FileName='doc_test.docx' FilePath='D:\\serveriAPI\\serveri\\sam\n" +
                "ple_data' PatternName='계좌 번호' PatternGUID='da6d6cf9-c293-4ac2-a779-82fbe3c0113c' Repetition='10106' ><![CDATA[243-12\n" +
                "-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727\n" +
                "243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-004727 243-12-0\n" +
                "04727 243-]]></Pattern><Pattern PatternName='UnsupportedDataType' Repetition='10106'></Pattern></PatternList>";

        String fileFullPath = SomansaDLPConfig.SOMANSA_DIR_PAHT+ fileInfo.getDirPath() + File.separator + fileInfo.getFileName();
        LOGGER.info("fileFullPath is : {}" , fileFullPath);
        DetectType detectType = DetectType.NON_DETECTABLE;

        PatternList patternList = parsingXML(source);

        minimumQuantityData.setDetectImpossibility(1L);
        LOGGER.info("fileFullPath is : {}" , fileFullPath);

        if(patternList.getException() != null) {
            detectType = DetectType.NON_DETECTABLE;
        }else if(patternList.getPatternList() == null){
            detectType = DetectType.NOT_DETECTABLE;
        }else if(patternList.getPatternList() != null ){
            detectType = DetectType.NOT_DETECTABLE;
            SomansaMinimumQuantityData fileInfoQuntiData = new SomansaMinimumQuantityData();
            for(Pattern pattern : patternList.getPatternList()) {
                MinimumQuantityCode quantityCode = MinimumQuantityCode.getQuantityCode(pattern.getPatternName());
                Long currentQuantity = pattern.getRepetition();
                Long minimumQuantity = this.getMinimumQuantityData(minimumQuantityData, quantityCode);
                detectType = this.isPrivacyInfo(currentQuantity, minimumQuantity, detectType);
                this.setQuantityData(fileInfoQuntiData, quantityCode, currentQuantity);
            }
            fileInfoQuntiData.setQuntityDataResult();
            //검출 불가 정보가 있으면 검출 불가 상태로 변경
            if(fileInfoQuntiData.getDetectImpossibility() > 0) detectType = DetectType.NON_DETECTABLE;
            fileInfo.setQuantityData(fileInfoQuntiData);
        }
        fileInfo.setPrivacyInfo(detectType);
        return fileInfo;
    }

    public boolean isDlpCompl(List<FileInfo> fileList) {
        int cnt = 0;
        for(FileInfo fileInfo: fileList) {
            String s = SomansaDLPConfig.SOMANSA_DIR_PAHT + fileInfo.getDirPath() + File.separator + fileInfo.getFileName();
            LOGGER.info("isDLPCompl : {}", s);
            File file1 = new File(s);
            if(file1.exists()) {
                cnt ++;
            }
        }
        return fileList.size() == cnt;
    }

    public String getDefaultDLPPath() {
        return SomansaDLPConfig.SOMANSA_DIR_PAHT;
    }

    /**
     *
     * 임계치 값을 불러오는 메소드
     *
     * @param minimumQuantityData
     * @param quantityCode
     * @return
     */
    private  Long getMinimumQuantityData(MinimumQuantityData minimumQuantityData, MinimumQuantityCode quantityCode) {
        Long minimumQuantity = 0L;
        try {
            minimumQuantity =
                    (Long) minimumQuantityData.getClass()
                            .getMethod("get" + quantityCode.getMemberName())
                            .invoke(minimumQuantityData);
        } catch (NoSuchMethodException e) {
            LOGGER.error("parsingError", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("parsingError", e);
        } catch (InvocationTargetException e) {
            LOGGER.error("parsingError", e);
        }
        return minimumQuantity;
    }

    /**
     * 임계치 이상이면 개인정보 검출대상 파일
     *
     * @param currentQuantity
     * @param minimumQuantity
     * @return
     */
    private DetectType isPrivacyInfo(Long currentQuantity, Long minimumQuantity, DetectType privacyInfo) {
        System.out.println("isPrivacyinfo.. : " + currentQuantity + " , mini : " + minimumQuantity);
        DetectType detectType;
        if(privacyInfo == null) {
            detectType = DetectType.NOT_DETECTABLE;
        }else {
            detectType = privacyInfo;
        }
        if(minimumQuantity != -1L && currentQuantity >= minimumQuantity) {
            detectType = DetectType.BE_DETECTED;
        }
        return detectType;
    }

    private  void setQuantityData(SomansaMinimumQuantityData minimumQuantityData, MinimumQuantityCode quantityCode, Long data) {
        try {
            minimumQuantityData.getClass()
                    .getMethod("set" + quantityCode.getMemberName(), Long.class)
                    .invoke(minimumQuantityData, data);
        } catch (NoSuchMethodException e) {
            LOGGER.error("parsingError", e);
        } catch (IllegalAccessException e) {
            LOGGER.error("parsingError", e);
        } catch (InvocationTargetException e) {
            LOGGER.error("parsingError", e);
        }
    }

    public ResultToList detectPrivacyInfo(DLPRequestDTO dlpRequestDTO) {
        String defaultDLPPath = this.getDefaultDLPPath();
        ResultToList resultToList = new ResultToList();
        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        Set<String> dirSet = new HashSet<>();
        for(DLPServiceDTO dlpServiceDTO : dlpRequestDTO.getDlpReqDTOS()) {
            ResultServiceReq resultServiceReq
                    = new ResultServiceReq(dlpServiceDTO.getSvcId(), MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE);
            List<FileInfo> fileInfos = new ArrayList<>();
            if(dlpServiceDTO.getFileNameList().size() == 0 ) {
                resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
            }
            for(FileInfo fileInfo: dlpServiceDTO.getFileNameList()){
                String targetDirPath = defaultDLPPath + File.separator + fileInfo.getDirPath();
                if(!Files.exists(Paths.get(targetDirPath))) {
                    new File(targetDirPath).mkdir();
                }
                //원본 파일 -> DRM 복호화
                int drmResult = 52315;
                LOGGER.info("DLP DRM RESULT {}, {}", fileInfo.getFileName(), drmResult);
                if(drmResult == 0) {
                    fileInfo = this.parsingContent(fileInfo, dlpServiceDTO.getQuantityData(), dlpRequestDTO.getCharSet());
                }else if(drmResult == 52315) {
                    //원본파일 DLP 검사 폴더로 복사
                    try {
                        Files.copy(Paths.get(MarkAnyConfig.DRMFILE_DIR + fileInfo.getDirPath() + File.separator + fileInfo.getFileName()),
                                Paths.get(targetDirPath + File.separator + fileInfo.getFileName()), StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        LOGGER.error("File copy error {}, tar {}",
                                MarkAnyConfig.DRMFILE_DIR + fileInfo.getDirPath() + File.separator + fileInfo.getFileName(),
                                targetDirPath + File.separator + fileInfo.getFileName());
                    }
                    fileInfo = this.parsingContent(fileInfo, dlpServiceDTO.getQuantityData(), dlpRequestDTO.getCharSet());
                }else {
                    fileInfo.setPrivacyInfo(DetectType.NON_DETECTABLE);
                }
                fileInfos.add(fileInfo);
                dirSet.add(targetDirPath);
            }
            resultServiceReq.setResult(fileInfos);
            resultServiceReqs.add(resultServiceReq);
        }
        resultToList.setResult(resultServiceReqs);
//        try {
//            for(String dirPath : dirSet) {
//                if(Files.exists(Paths.get(dirPath))) {
//                    Files.walk(Paths.get(dirPath))
//                        .map(Path::toFile)
//                        .forEach(file -> {
//                            LOGGER.info("FILE PAHT : {}" , file.getAbsoluteFile() );
//                            file.delete();
//                        });
//                    new File(dirPath).deleteOnExit();
//                }
//            }
//        } catch (IOException e) {
//            LOGGER.error("DLP File delete Exception " , e.getCause());
//        }
        return resultToList;
    }
}
