package com.solupia.esr.web.api.kyobo.drm.controller;

import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.drm.dto.*;
import com.solupia.esr.web.api.kyobo.drm.service.DRMRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by solupia on 2017. 4. 7..
 */
@RestController
public class DrmWebController {

    @Autowired
    private DRMRequestService drmRequestService;

    /**
     * 업로드된 파일을 DRM 모듈을 사용하여 '열기' 만 가능한 문서로 변경한다
     *
     * @param requestDTO
     */
    @PostMapping(value = "/drm/extractDRMFiles", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList extractDRMFiles(@RequestBody DRMRequestDTOS<DRMServiceDTO> requestDTO) {
        return drmRequestService.extractDRMFiles(requestDTO);
    }

    /**
     * 업로드된 파일을 DRM 모듈을 사용하여 '워터마크' 예외 문서로 변경한다
     *
     * @param requestDTO
     */
    @PostMapping(value = "/drm/extractDRMWater", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList extractDRMWater(@RequestBody DRMRequestDTOS<DRMServiceDTO> requestDTO) {
        return drmRequestService.extractDRMWates(requestDTO);
    }

    /**
     * 파일은 다운로드 하기 위하여 복호화 한다
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/drm/drmDocDecs", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList drmDocDecs(@RequestBody DRMRequestDTOS<DRMFileInfoDTO> requestDTO) {
        return drmRequestService.drmDocDecs(requestDTO);
    }

    /**
     * 화면 캡쳐 방지
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/drm/drmExecScreenCapture", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList drmExecScreenCapture(@RequestBody DRMRequestDTOS<DrmDBServiceDTOS> requestDTO) {
        return drmRequestService.drmExecScreenCapture(requestDTO);
    }

    /**
     * 노트북 반출
     * @param drmRequestDTO
     * @return
     */
    @PostMapping(value = "/drm/drmExecLaptop", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList drmExecLaptop(@RequestBody DRMRequestDTOS<DrmDBServiceDTOS<DrmRequestLaptopDTO>> drmRequestDTO) {
        return drmRequestService.drmExecLaptop(drmRequestDTO);
    }


}
