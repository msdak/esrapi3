package com.solupia.esr.web.api.kyobo.drm.dto;

import com.solupia.dlp.comm.dto.FileInfo;
import com.solupia.esr.web.api.kyobo.pcexec.dto.PcExecTakeOutFileinfoDTO;
import lombok.Data;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DRMFileInfoDTO {

    private Long   fileId;
    private String dirPath;
    private String fileName;
    private String orgFileName;
    private String encOrgFileName;
    private String ext;

    private Long decryptFileSize;
    private Long encryptFileSize;
    private Long compactFileSize;

    public static DRMFileInfoDTO of(FileInfo fileInfo) {
        DRMFileInfoDTO drmFileInfoDTO = new DRMFileInfoDTO();
        drmFileInfoDTO.setFileId(fileInfo.getFileId());
        drmFileInfoDTO.setDirPath(fileInfo.getDirPath());
        drmFileInfoDTO.setFileName(fileInfo.getFileName());
        return drmFileInfoDTO;
    }

    public static DRMFileInfoDTO of(PcExecTakeOutFileinfoDTO pcExecTakeOutFileinfoDTO) {
        DRMFileInfoDTO drmFileInfoDTO = new DRMFileInfoDTO();
        drmFileInfoDTO.setFileId(pcExecTakeOutFileinfoDTO.getFileId());
        drmFileInfoDTO.setDirPath(pcExecTakeOutFileinfoDTO.getDirPath());
        drmFileInfoDTO.setFileName(pcExecTakeOutFileinfoDTO.getFileName());
        drmFileInfoDTO.setExt(pcExecTakeOutFileinfoDTO.getExt());
        drmFileInfoDTO.setOrgFileName(pcExecTakeOutFileinfoDTO.getOrgFileName());
        return drmFileInfoDTO;
    }
}
