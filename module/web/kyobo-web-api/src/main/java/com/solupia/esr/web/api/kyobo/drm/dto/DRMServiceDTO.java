package com.solupia.esr.web.api.kyobo.drm.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class DRMServiceDTO {

    private Long svcId;
    private String chnCode;
    private String userId;
    private List<DRMFileInfoDTO> drmFileInfoDTOS;

}
