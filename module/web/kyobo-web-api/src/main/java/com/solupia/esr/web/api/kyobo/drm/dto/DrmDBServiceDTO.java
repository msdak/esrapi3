package com.solupia.esr.web.api.kyobo.drm.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by solupia on 2017. 4. 17..
 */
@Getter @Setter
public class DrmDBServiceDTO {
    private String userId;
    private String aprvUserId;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}
