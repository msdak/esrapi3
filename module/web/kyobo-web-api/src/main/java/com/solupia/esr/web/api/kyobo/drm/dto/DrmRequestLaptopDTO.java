package com.solupia.esr.web.api.kyobo.drm.dto;

import lombok.Data;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Data
public class DrmRequestLaptopDTO extends DrmDBServiceDTO {
    private LaptopType type;
}
