package com.solupia.esr.web.api.kyobo.drm.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Getter
@Setter
public class DrmResponseLaptoDTO {

    private String code;
    private String message;

    public DrmResponseLaptoDTO(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
