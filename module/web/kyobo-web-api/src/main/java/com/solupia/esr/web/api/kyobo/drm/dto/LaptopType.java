package com.solupia.esr.web.api.kyobo.drm.dto;

/**
 * @author jason, Moon.
 * @since 2017-03-31.
 */
public enum LaptopType {
    READ("2"), WRITE("3");

    private String laptopTypeVal;

    LaptopType(String laptopTypeVal) {
        this.laptopTypeVal = laptopTypeVal;
    }

    public String getLaptopTypeVal() {
        return this.laptopTypeVal;
    }
}
