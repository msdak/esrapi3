package com.solupia.esr.web.api.kyobo.drm.dto;

import lombok.Data;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Data
public class ResultDRMFileinfoDTO {
    
    private Long fileId;
    private String fileName;
    private String code;

    private Long decryptFileSize;
    private Long encryptFileSize;
    private Long compactFileSize;

    public ResultDRMFileinfoDTO() {
    }

    public ResultDRMFileinfoDTO(String code) {
        this.code = code;
    }

    public ResultDRMFileinfoDTO(Long fileId, String code) {
        this.fileId = fileId;
        this.code = code;
    }

    public ResultDRMFileinfoDTO(Long fileId, String code, Long decryptFileSize, Long encryptFileSize, Long compactFileSize) {
        this.fileId = fileId;
        this.code = code;
        this.decryptFileSize = decryptFileSize;
        this.encryptFileSize = encryptFileSize;
        this.compactFileSize = compactFileSize;
    }

    public ResultDRMFileinfoDTO(Long fileId, String fileName, String code) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.code = code;
    }
}
