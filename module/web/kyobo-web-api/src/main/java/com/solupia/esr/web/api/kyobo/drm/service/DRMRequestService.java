package com.solupia.esr.web.api.kyobo.drm.service;

import com.solupia.esr.web.api.kyobo.common.ResultServiceReq;
import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.TimeFormat;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.common.util.DrmUtil;
import com.solupia.esr.web.api.kyobo.drm.dto.*;
import com.solupia.esr.web.api.kyobo.pcexec.dto.PcExecTakeOutFileinfoDTO;
import com.solupia.esr.web.api.kyobo.pcexec.service.PcExecWebService;
import com.solupia.markany.config.MarkAnyConfig;
import com.solupia.markany.dec.MarkAnyDecUtils;
import com.solupia.markany.service.MarkAnyDBService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by solupia on 2017. 4. 7..
 */
@Service
public class DRMRequestService {

    @Autowired
    private MarkAnyDBService markAnyDBService;

    @Autowired
    private PcExecWebService pcExecWebService;

    private static final Logger LOGGER = LoggerFactory.getLogger(DRMRequestService.class);

    /**
     * 열기 가능 파일로 암호화 한다
     * @param drmFileInfoDTO
     * @param userId
     * @return
     */
    private int extractDRMF(DRMFileInfoDTO drmFileInfoDTO, String userId) {
        String filePath = drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName();
        return MarkAnyDecUtils.toEncOpen(filePath, userId, drmFileInfoDTO.getOrgFileName(), drmFileInfoDTO.getExt());
    }

    /**
     * 워터마크 파일로 암호화 한다
     * @param drmFileInfoDTO
     * @param userId
     * @return
     */
    private int extractDRMWaster(DRMFileInfoDTO drmFileInfoDTO, String userId) {
        String filePath = drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName();
        String dirPath = MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath();
        if(!Files.exists(Paths.get(dirPath))) {
            new File(dirPath).mkdir();
        }
        return MarkAnyDecUtils.toWaterMark(filePath, userId, drmFileInfoDTO.getOrgFileName(), drmFileInfoDTO.getExt());
    }

    public int cpDrmDocDec(DRMFileInfoDTO drmFileInfoDTO) {
        String dirPath = MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath();
        File dir = new File(dirPath);
//        if(dir.exists()) {
//            try {
//                Files.walk(Paths.get(dirPath))
//                        .map(Path::toFile)
//                        .sorted((o1, o2) -> -o1.compareTo(o2))
//                        .forEach(File::delete);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
        if(!dir.exists()) dir.mkdir();
        return drmDocDec(drmFileInfoDTO);
    }

    private int drmDocDec(DRMFileInfoDTO drmFileInfoDTO) {
        String filePath = drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName();
        return MarkAnyDecUtils.extractToSource(filePath);
    }

    public int drmDocExternDec(DRMFileInfoDTO drmFileInfoDTO, String externFilePath) {
        String filePath = drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName();
        return MarkAnyDecUtils.extractToExternSource(filePath, externFilePath);
    }

    /**
     *
     * 파일 복호화
     *
     * @param requestDTO
     * @return     {status : 200,
     *             message : 성공,
     *             result : [
     *                  {
     *                      fileId : 2,
     *                      status : 200
     *                  },
     *                  {
     *                      fileId : 1,
     *                      status : 200
     *                  }
     *             ]}
     */
    public ResultToList drmDocDecs(DRMRequestDTOS<DRMFileInfoDTO> requestDTO) {
        ResultToList resultToLists = new ResultToList();
        List<ResultDRMFileinfoDTO> resultDRMFileinfoDTOS = new ArrayList<>();
        for(DRMFileInfoDTO drmFileInfoDTO : requestDTO.getDrmServiceDTOS()){
            int result = this.cpDrmDocDec(drmFileInfoDTO);
            String code = CodeConstant.SUCCESS_CODE;
            if(result != 0) {
                code = CodeConstant.FAIL_CODE;
            }
            resultDRMFileinfoDTOS.add(new ResultDRMFileinfoDTO(drmFileInfoDTO.getFileId(), code));
        }
        resultToLists.setResult(resultDRMFileinfoDTOS);
        return resultToLists;
    }

    public int preChgDrmOpenFile(DRMServiceDTO drmServiceDTO, DRMFileInfoDTO drmFileInfoDTO) {
        int result = 0;
        switch (drmServiceDTO.getChnCode()){
            case "FILE_TAKEOUT" :
            case "FILE_TAKEOUT_CD" :
                /** 원본 파일 다운로드 폴더로 복사*/
                File dir = new File(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath());
                LOGGER.info("FILE__post dirpaht {}", dir.getAbsolutePath());
                if(!dir.exists()) dir.mkdir();
                try {
                    Files.copy(
                            Paths.get(MarkAnyConfig.DRMFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName()),
                            Paths.get(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName()),
                            StandardCopyOption.REPLACE_EXISTING);
                    LOGGER.info("FILE__post sourcePath {}", MarkAnyConfig.DRMFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                    LOGGER.info("FILE__post targetPath {}", MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                } catch (IOException e) {
                    LOGGER.error("File takeout copy error", e.fillInStackTrace());
                    result = 1;
                    break;
                }
            default:
                if(DrmUtil.isSupExt(drmFileInfoDTO.getExt())) {
                    result = this.extractDRMF(drmFileInfoDTO, drmServiceDTO.getUserId());
                    LOGGER.info("MarkAny..Result chgDrmOpenFile : {} :: {}", drmFileInfoDTO.getFileName() , result);
                } else {
                    result = 0;
                    LOGGER.info("MarkAny..Pass File Result chgDrmOpenFile : {} :: {}", drmFileInfoDTO.getFileName() , result);
                }
        }
        return result;
    }

    /**
     * @param requestDTO
     * @return
     *      [{
     *             svcId : 1,
     *             status : 200,
     *             message : 성공,
     *             result : [
     *                  {
     *                      fileId : 2,
     *                      status : 200
     *                  },
     *                  {
     *                      fileId : 1,
     *                      status : 200
     *                  }
     *             ]
     *      }]
     */
    private List<ResultServiceReq> chgDrmOpenFile(DRMRequestDTOS<DRMServiceDTO> requestDTO) {
        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        for(DRMServiceDTO drmServiceDTO : requestDTO.getDrmServiceDTOS()) {
            ResultServiceReq resultServiceReq
                    = new ResultServiceReq(drmServiceDTO.getSvcId(),
                        MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE, drmServiceDTO.getChnCode());
            List<ResultDRMFileinfoDTO> resultDRMFileinfoDTOS = new ArrayList<>();

            if(drmServiceDTO.getDrmFileInfoDTOS().size() == 0) {
                resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
            }
            for(DRMFileInfoDTO drmFileInfoDTO : drmServiceDTO.getDrmFileInfoDTOS()){

                drmFileInfoDTO.setCompactFileSize(0L);
                drmFileInfoDTO.setEncryptFileSize(0L);
                drmFileInfoDTO.setDecryptFileSize(0L);

                int result = this.preChgDrmOpenFile(drmServiceDTO, drmFileInfoDTO);
                String code = CodeConstant.SUCCESS_CODE;

                if(result == 0) {
                    //암호화 파일 사이즈 알아내기
                    String filePath = MarkAnyConfig.DRMFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName();
                    File encFile = new File(filePath);
                    drmFileInfoDTO.setEncryptFileSize(encFile.length());
                    LOGGER.info("enc File Size : {}" , encFile.length());
                    //후처리
                    switch (drmServiceDTO.getChnCode()){
                        case "WATER_MARK" :
                            //워터마크 파일 사이즈 알아내기
                            result = this.extractDRMWaster(drmFileInfoDTO, drmServiceDTO.getUserId());
                            File waterFile = new File(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                            drmFileInfoDTO.setDecryptFileSize(waterFile.length());
                            LOGGER.info("water mark File Size : {}" , waterFile.length());
                            break;
                        case "CALCULATION_FILE":
                        case "DOC" :
                            //복호화 파일 사이즈 알아내기
                            result = this.cpDrmDocDec(drmFileInfoDTO);
                            File decFile = new File(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                            LOGGER.info("Dec File.. {}", MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                            drmFileInfoDTO.setDecryptFileSize(decFile.length());
                            LOGGER.info("doc File Size : {}" , decFile.length());
                            break;
                        case "FILE_TAKEOUT":
                            //ZIP파일 사이즈 알아내기
                            try {
                                result = pcExecWebService.fileTake(PcExecTakeOutFileinfoDTO.of(drmFileInfoDTO, drmServiceDTO.getUserId()));
                                File zipFile =
                                        new File(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath()
                                                + File.separator + drmFileInfoDTO.getFileName().substring(0, drmFileInfoDTO.getFileName().lastIndexOf(".")) + ".zip");
                                drmFileInfoDTO.setCompactFileSize(zipFile.length());
                                LOGGER.info("file takeout File Size : {}" , zipFile.length());
                            } catch (UnsupportedEncodingException e) {
                                LOGGER.error("OrgFileName chgError,,, ", e.fillInStackTrace());
                            }finally {
                                break;
                            }
                        case "FILE_TAKEOUT_CD" :
                            //원본 파일 사이즈 알아내기
                            File orgFile = new File(MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                            LOGGER.info("Org File.. {}", MarkAnyConfig.ENCFILE_DIR + drmFileInfoDTO.getDirPath() + File.separator + drmFileInfoDTO.getFileName());
                            drmFileInfoDTO.setDecryptFileSize(orgFile.length());
                            LOGGER.info("Org File Size : {}" , orgFile.length());
                    }
                }
                /* 마크애니 암호화 후 결과 값이 성공이 아니라면 */
                /* 해당 파일 실패 처리 및 svcReqId 실패 처리*/
                if(result != 0) {
                    /* 서비스 Req 실패 처리  */
                    resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                    resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
                    code = CodeConstant.FAIL_CODE;
                }
                ResultDRMFileinfoDTO resultDRMFileinfoDTO
                        = new ResultDRMFileinfoDTO(drmFileInfoDTO.getFileId(), code,
                                    drmFileInfoDTO.getDecryptFileSize(), drmFileInfoDTO.getEncryptFileSize(), drmFileInfoDTO.getCompactFileSize());
                resultDRMFileinfoDTOS.add(resultDRMFileinfoDTO);
            }
            resultServiceReq.setResult(resultDRMFileinfoDTOS);
            resultServiceReqs.add(resultServiceReq);
        }
        return resultServiceReqs;
    }

    public ResultToList extractDRMFiles(DRMRequestDTOS requestDTO) {
        ResultToList resultToList = new ResultToList();
        resultToList.setResult(this.chgDrmOpenFile(requestDTO));
        return resultToList;
    }

    public ResultToList extractDRMWates(DRMRequestDTOS requestDTO) {
        ResultToList resultToList = new ResultToList();
        resultToList.setResult(this.extractDRMWater(requestDTO));
        return resultToList;
    }

    private List<ResultServiceReq> extractDRMWater(DRMRequestDTOS<DRMServiceDTO> requestDTO) {

        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        for(DRMServiceDTO drmServiceDTO : requestDTO.getDrmServiceDTOS()) {
            ResultServiceReq resultServiceReq
                    = new ResultServiceReq(drmServiceDTO.getSvcId(), MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE);
            List<ResultDRMFileinfoDTO> resultDRMFileinfoDTOS = new ArrayList<>();
            if(drmServiceDTO.getDrmFileInfoDTOS().size() == 0) {
                resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
            }
            for(DRMFileInfoDTO drmFileInfoDTO : drmServiceDTO.getDrmFileInfoDTOS()){
                int result = this.extractDRMWaster(drmFileInfoDTO, drmServiceDTO.getUserId());
                LOGGER.info("MarkAny..Result extractDRMWater : {} :: {}", drmFileInfoDTO.getFileName() , result);
                String code = CodeConstant.SUCCESS_CODE;
                /* 마크애니 암호화 후 결과 값이 성공이 아니라면 */
                /* 해당 파일 실패 처리 및 svcReqId 실패 처리*/
                if(result != 0) {
                    /* 서비스 Req 실패 처리  */
                    resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                    resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
                    code = CodeConstant.FAIL_CODE;
                }
                ResultDRMFileinfoDTO resultDRMFileinfoDTO = new ResultDRMFileinfoDTO(drmFileInfoDTO.getFileId(), code);
                resultDRMFileinfoDTOS.add(resultDRMFileinfoDTO);
            }
            resultServiceReq.setResult(resultDRMFileinfoDTOS);
            resultServiceReqs.add(resultServiceReq);
        }
        return resultServiceReqs;
    }

    public ResultToList drmExecLaptop(DRMRequestDTOS<DrmDBServiceDTOS<DrmRequestLaptopDTO>> drmRequestDTO) {

        ResultToList resultToList = new ResultToList();
        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        for(DrmDBServiceDTOS<DrmRequestLaptopDTO> drmDBServiceDTOS : drmRequestDTO.getDrmServiceDTOS()) {

            ResultServiceReq resultServiceReq = new ResultServiceReq(
                    drmDBServiceDTOS.getSvcId(), MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE, drmDBServiceDTOS.getChnCode());

            List<DrmResponseLaptoDTO> drmResponseLaptoDTOS = new ArrayList<>();

            for(DrmRequestLaptopDTO drmRequestLaptopDTO : drmDBServiceDTOS.getDrmServiceDTOS()){

                DrmResponseLaptoDTO drmResponseLaptoDTO = new DrmResponseLaptoDTO(CodeConstant.SUCCESS_CODE, MsgConstant.SUCCESS_MSG);
                LOGGER.info("laptop .. {}", drmRequestLaptopDTO.toString());
                try{
                    int i = markAnyDBService
                            .execInsertAppCrrpc(
                                    drmRequestLaptopDTO.getUserId(),
                                    drmRequestLaptopDTO.getType().getLaptopTypeVal(),
                                    TimeFormat.dtfYYYMMDD.format(drmRequestLaptopDTO.getStartDate()),
                                    TimeFormat.dtfYYYMMDD.format(drmRequestLaptopDTO.getEndDate()),
                                    drmRequestLaptopDTO.getAprvUserId());
                }catch (Exception e) {
                    LOGGER.error("DRM...LapTop Procedure Error ", e.fillInStackTrace());
                    drmResponseLaptoDTO.setCode(CodeConstant.FAIL_CODE);
                    drmResponseLaptoDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);

                    resultServiceReq.setCode(CodeConstant.FAIL_CODE);
                    resultServiceReq.setMessage(MsgConstant.FAIL_MSG);
                }
                drmResponseLaptoDTOS.add(drmResponseLaptoDTO);
            }
            resultServiceReq.setResult(drmResponseLaptoDTOS);
            resultServiceReqs.add(resultServiceReq);
        }
        resultToList.setResult(resultServiceReqs);
        return resultToList;
    }

    /**
     *
     * @param requestDTO
     * @return
     */
    public ResultToList drmExecScreenCapture(DRMRequestDTOS<DrmDBServiceDTOS> requestDTO) {
        ResultToList resultToList = new ResultToList();
        List<ResultServiceReq> resultServiceReqs = new ArrayList<>();
        for(DrmDBServiceDTOS<DrmDBServiceDTO> drmDBServiceDTOS : requestDTO.getDrmServiceDTOS()) {
            ResultServiceReq resultServiceReq = new ResultServiceReq(
                    drmDBServiceDTOS.getSvcId(), MsgConstant.SUCCESS_MSG, CodeConstant.SUCCESS_CODE, drmDBServiceDTOS.getChnCode());
            List<DrmResponseLaptoDTO> resultDrmDBServiceDTOS = new ArrayList<>();

            for(DrmDBServiceDTO drmDBServiceDTO : drmDBServiceDTOS.getDrmServiceDTOS()) {
                DrmResponseLaptoDTO drmResponseLaptoDTO = new DrmResponseLaptoDTO(CodeConstant.SUCCESS_CODE, MsgConstant.SUCCESS_MSG);
                try{
                    markAnyDBService
                            .execDsAgentSetCcfvalue(
                                    "NewInsuranceImgEndDate",
                                    TimeFormat.dtfYYYMMDD.format(drmDBServiceDTO.getEndDate()),
                                    "NewInsurance Imgsafer End Date",
                                    drmDBServiceDTO.getUserId());
                    markAnyDBService
                            .execDsAgentSetCcfvalue(
                                    "NewInsuranceImgStartDate",
                                    TimeFormat.dtfYYYMMDD.format(drmDBServiceDTO.getStartDate()),
                                    "NewInsurance Imgsafer Start Date",
                                    drmDBServiceDTO.getUserId());
                    markAnyDBService
                            .execDsAgentSetCcfvalue(
                                    "NewInsuranceImgSAFER",
                                    "0",
                                    "NewInsurance Imgsafer",
                                    drmDBServiceDTO.getUserId());
                }catch (Exception e) {
                    LOGGER.error("DRM...ScreenCapture Procedure Error ", e.fillInStackTrace());
                    drmResponseLaptoDTO.setCode(CodeConstant.FAIL_CODE);
                    drmResponseLaptoDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
                }
                resultDrmDBServiceDTOS.add(drmResponseLaptoDTO);
            }
            resultServiceReq.setResult(resultDrmDBServiceDTOS);
            resultServiceReqs.add(resultServiceReq);
        }
        resultToList.setResult(resultServiceReqs);
        return resultToList;
    }
}
