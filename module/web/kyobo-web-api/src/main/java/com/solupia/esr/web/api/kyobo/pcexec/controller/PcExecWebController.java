package com.solupia.esr.web.api.kyobo.pcexec.controller;

import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToObject;
import com.solupia.esr.web.api.kyobo.pcexec.dto.*;
import com.solupia.esr.web.api.kyobo.pcexec.service.PcExecWebService;
import com.solupia.safepc.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by solupia on 2017. 4. 14..
 */
@RestController
public class PcExecWebController {

    @Autowired
    private PcExecWebService pcExecWebService;

    /**
     * 일반 /보안 USB 사용 신청
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/usbDeviceProposal", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList postUSBDeviceProposal(@RequestBody PcExecRequestDTOS<PcExecServiceDTO<DeviceRegist>> requestDTO) {
        return pcExecWebService.postUSBDeviceProposal(requestDTO.getPcExecServiceDTOS());
    }

    /**
     * 보안 USB 외부 반출
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/takeoutUsbDeviceProposal", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList takeoutUsbDeviceProposal(@RequestBody PcExecRequestDTOS<PcExecServiceDTO<ExternDeviceRegist>> requestDTO) {
        return pcExecWebService.takeoutUsbDeviceProposal(requestDTO.getPcExecServiceDTOS());
    }

    /**
     * 매체 제어
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/mediaControl", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList postMediaControl(@RequestBody PcExecRequestDTOS<PcExecServiceDTO<RequestApplyPolicyDTO>> requestDTO) {
        return pcExecWebService.postMediaControl(requestDTO.getPcExecServiceDTOS());
    }

    /**
     * 반출 가능 보안 USB 조회
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/usbDeviceInfos", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList postUsbDeviceInfos(@RequestBody PcExecRequestDTO<RequestUSB> requestDTO) {
        return pcExecWebService.postUSBDeviceInfos(requestDTO);
    }

    /**
     * 보안 USB 정보 조회
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/usbPolicyInfos", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList postUsbPolicyInfos(@RequestBody PcExecRequestDTO<RequestPolicyUSB> requestDTO) {
        return pcExecWebService.postUsbPolicyInfos(requestDTO);
    }

    /**
     * 보안 USB 회수하기
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/returnedUsb", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToObject<ResponseReturnedUsb> postReturnedUsb(@RequestBody PcExecRequestDTO<RequestUSB> requestDTO) {
        return pcExecWebService.postReturnedUsb(requestDTO);
    }

    /**
     * 네트워크 잠금 해제
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/releaseNetworkLock", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList releaseNetworkLock(@RequestBody PcExecRequestDTOS<PcExecServiceDTO<PcExecNetworkLock>> requestDTO) {
        return pcExecWebService.releaseNetworkLock(requestDTO.getPcExecServiceDTOS());
    }

    /**
     * 보안 USB 파일 반출 용 ZIP
     * @param requestDTO
     * @return
     */
    @PostMapping(value = "/pcexec/fileTakeout", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public ResultToList fileTakeout(@RequestBody PcExecRequestDTOS<PcExecTakeOutFileinfoDTO> requestDTO) {
        return pcExecWebService.fileTakeout(requestDTO);
    }

}
