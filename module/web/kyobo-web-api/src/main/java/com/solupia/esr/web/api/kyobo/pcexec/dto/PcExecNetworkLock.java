package com.solupia.esr.web.api.kyobo.pcexec.dto;

import lombok.Data;

/**
 * Created by solupia on 2017. 4. 19..
 */
@Data
public class PcExecNetworkLock {
    private String auditUser;
    private String userId;
}
