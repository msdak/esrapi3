package com.solupia.esr.web.api.kyobo.pcexec.dto;

import lombok.Data;

/**
 * Created by solupia on 2017. 4. 18..
 */
@Data
public class PcExecRequestDTO<T> {
    private T pcExecRequestDTO;
}
