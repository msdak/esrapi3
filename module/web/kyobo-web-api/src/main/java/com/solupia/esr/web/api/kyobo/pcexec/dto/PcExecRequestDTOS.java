package com.solupia.esr.web.api.kyobo.pcexec.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Data
public class PcExecRequestDTOS<T> {

    private List<T> pcExecServiceDTOS;
}
