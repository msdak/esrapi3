package com.solupia.esr.web.api.kyobo.pcexec.dto;

import lombok.Data;

import java.util.List;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Data
public class PcExecServiceDTO<T> {

    private Long svcId;
    private String chnCode;
    private String code;
    private String message;
    private List<T> deviceRegists;

}
