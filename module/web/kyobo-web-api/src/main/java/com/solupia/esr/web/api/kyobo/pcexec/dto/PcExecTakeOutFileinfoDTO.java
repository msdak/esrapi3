package com.solupia.esr.web.api.kyobo.pcexec.dto;

import com.solupia.esr.web.api.kyobo.drm.dto.DRMFileInfoDTO;
import lombok.Data;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by solupia on 2017. 4. 20..
 */
@Data
public class PcExecTakeOutFileinfoDTO {
    private Long   fileId;
    private String dirPath;
    private String fileName;
    private String orgFileName;
    private String ext;
    private String userId;

    public static PcExecTakeOutFileinfoDTO of(DRMFileInfoDTO drmFileInfoDTO, String userId) throws UnsupportedEncodingException {
        PcExecTakeOutFileinfoDTO pcExecTakeOutFileinfoDTO = new PcExecTakeOutFileinfoDTO();

        pcExecTakeOutFileinfoDTO.setDirPath(drmFileInfoDTO.getDirPath());
        pcExecTakeOutFileinfoDTO.setExt(drmFileInfoDTO.getExt());
        pcExecTakeOutFileinfoDTO.setFileId(drmFileInfoDTO.getFileId());
        pcExecTakeOutFileinfoDTO.setFileName(drmFileInfoDTO.getFileName());
        pcExecTakeOutFileinfoDTO.setOrgFileName(URLDecoder.decode(drmFileInfoDTO.getEncOrgFileName(), "utf-8"));
        pcExecTakeOutFileinfoDTO.setUserId(userId);
        return pcExecTakeOutFileinfoDTO;
    }
}
