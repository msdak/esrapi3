package com.solupia.esr.web.api.kyobo.pcexec.dto;

import com.solupia.esr.web.api.kyobo.common.dto.RequestType;
import com.solupia.esr.web.api.kyobo.common.dto.YesOrNo;
import com.solupia.safepc.dto.RequestApplyPolicy;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by solupia on 2017. 4. 17..
 */
@Getter @Setter
public class RequestApplyPolicyDTO extends RequestApplyPolicy {

    private YesOrNo wifiUseYn;
    private RequestType ptbStrgReqType;
    private RequestType cdUseYn;
    private YesOrNo smartPhoneUseYn;

}
