package com.solupia.esr.web.api.kyobo.pcexec.service;

import com.solupia.esr.web.api.kyobo.common.dto.RequestType;
import com.solupia.esr.web.api.kyobo.common.dto.YesOrNo;
import com.solupia.esr.web.api.kyobo.common.refrence.common.CodeConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.common.MsgConstant;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToList;
import com.solupia.esr.web.api.kyobo.common.refrence.dto.ResultToObject;
import com.solupia.esr.web.api.kyobo.drm.dto.ResultDRMFileinfoDTO;
import com.solupia.esr.web.api.kyobo.pcexec.dto.*;
import com.solupia.markany.config.MarkAnyConfig;
import com.solupia.safepc.common.CommonXMLParser;
import com.solupia.safepc.common.GenerateReqNo;
import com.solupia.safepc.common.USBMethodType;
import com.solupia.safepc.dto.*;
import com.solupia.safepc.service.SafePcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by solupia on 2017. 4. 14..
 */
@Service
public class PcExecWebService {

    @Autowired
    private SafePcService safePcService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PcExecWebService.class);

    public ResultToList takeoutUsbDeviceProposal(List<PcExecServiceDTO<ExternDeviceRegist>> pcExecServiceDTOS) {
        ResultToList resultToList = new ResultToList();

        for(PcExecServiceDTO<ExternDeviceRegist> pcExecServiceDTO : pcExecServiceDTOS) {
            pcExecServiceDTO.setCode(CodeConstant.SUCCESS_CODE);
            pcExecServiceDTO.setMessage(MsgConstant.SUCCESS_MSG);
            for(ExternDeviceRegist deviceRegist : pcExecServiceDTO.getDeviceRegists()) {
                PcExecResult deviceResult;
                try {
                    deviceRegist.setAppState("2");
                    deviceResult= safePcService.postUSBDeviceProposal(deviceRegist.toXml(), USBMethodType.TAKEOUT).get(0);

                    if(!deviceResult.getCode().equals("0")) {
                        pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
                        pcExecServiceDTO.setMessage(deviceResult.getMessage());
                        LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), deviceResult.getMessage());
                    }
                } catch (IOException e) {
                    pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
                    pcExecServiceDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
                    LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), e.getCause());
                }
            }
        }
        resultToList.setResult(pcExecServiceDTOS);
        return resultToList;

    }

    public ResultToList postUSBDeviceProposal(List<PcExecServiceDTO<DeviceRegist>> pcExecServiceDTOS) {
        ResultToList resultToList = new ResultToList();

        List<PcExecServiceDTO<PcExecResult>> pcExecResultList = new ArrayList<>();

        for(PcExecServiceDTO<DeviceRegist> pcExecServiceDTO : pcExecServiceDTOS) {
            PcExecServiceDTO<PcExecResult> tempDTO = new PcExecServiceDTO<>();
            List<PcExecResult> deviceResults = new ArrayList<>();
            tempDTO.setSvcId(pcExecServiceDTO.getSvcId());
            tempDTO.setCode(CodeConstant.SUCCESS_CODE);
            tempDTO.setMessage(MsgConstant.SUCCESS_MSG);

            for(DeviceRegist deviceRegist : pcExecServiceDTO.getDeviceRegists()) {
                PcExecResult deviceResult = new PcExecResult();
                try {
                    deviceRegist.setAppState("0");
                    List<PcExecResult> deviceTempResults = safePcService.postUSBDeviceProposal(deviceRegist.toXml(), USBMethodType.PROPOSAL);
                    if(deviceTempResults.size() == 1) {
                        deviceResult = deviceTempResults.get(0);
                        deviceResult.setCode("0");
                    }else {
                        deviceRegist.setAppState("2");
                        if(deviceRegist.getDType().toUpperCase().equals("P")){
                            deviceRegist.setStartDT(deviceRegist.getReqDT());
                            deviceRegist.setEndDT(LocalDateTime.of(2999, 12, 31, 00, 00));
                        }
                        deviceRegist.setReqNo(deviceTempResults.get(1).getReqNo());

                        List<PcExecResult> pcExecResults = safePcService.postUSBDeviceProposal(deviceRegist.toXml(), USBMethodType.PROPOSAL);

                        deviceResult.setCode(pcExecResults.get(0).getCode());
                        deviceResult.setMessage(pcExecResults.get(0).getMessage());
//                        deviceResult.setReqNo(pcExecResults.get(1).getReqNo());
                        deviceResult.setMgmtKey(pcExecResults.get(1).getMgmtKey());
                        deviceResults.add(deviceResult);
                    }

                    if(!deviceResult.getCode().equals("0")) {
                        tempDTO.setCode(CodeConstant.FAIL_CODE);
                        tempDTO.setMessage(deviceResult.getMessage());
                        LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), deviceResult.getMessage());
                    }
                } catch (IOException e) {
                    tempDTO.setCode(CodeConstant.FAIL_CODE);
                    tempDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
                    LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), e.getCause());
                }
            }
            tempDTO.setDeviceRegists(deviceResults);
            pcExecResultList.add(tempDTO);
        }
        resultToList.setResult(pcExecResultList);
        return resultToList;
    }

    public ResultToList postMediaControl(List<PcExecServiceDTO<RequestApplyPolicyDTO>> pcExecServiceDTOS) {
        ResultToList resultToList = new ResultToList();
        for(PcExecServiceDTO<RequestApplyPolicyDTO> pcExecServiceDTO : pcExecServiceDTOS) {
            pcExecServiceDTO.setCode(CodeConstant.SUCCESS_CODE);
            pcExecServiceDTO.setMessage(MsgConstant.SUCCESS_MSG);
            for(RequestApplyPolicyDTO requestApplyPolicyDTO : pcExecServiceDTO.getDeviceRegists()) {
                try {
                    requestApplyPolicyDTO.setReqNo(GenerateReqNo.getReqNo());
                    String postXML = this.makePostXMl(requestApplyPolicyDTO);
                    LOGGER.info("pcexec Post xml : {}", postXML);
                    PcExecResult pcExecResult = safePcService.postApplyPolicy(postXML);
                    if(!pcExecResult.getCode().equals("0")) {
                        pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
                        pcExecServiceDTO.setMessage(pcExecResult.getMessage());
                        LOGGER.error("PCEXEC_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), pcExecResult.getCode(), pcExecResult.getMessage());
                    }
                } catch (IOException e) {
                    pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
                    pcExecServiceDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
                    LOGGER.error("PCEXEC_FAIL {}, {}", pcExecServiceDTO.getSvcId(), e.getCause());
                }
            }
        }
        resultToList.setResult(pcExecServiceDTOS);
        return resultToList;
    }

    private boolean isDoubleType(int length) {
        return length == 2;
    }

    private String makePostXMl(RequestApplyPolicyDTO requestApplyPolicyDTO) throws IOException {
//        String source = "<?xml version='1.0' encoding='utf-8'?>\n" +
//                "<request>\n" +
//                "<user>연동할때ID</user>\n" +
//                "<password>연동할때PWD</password>\n" +
//                "<body>\n" +
//                "<command>SetUserRule</command>\n" +
//                "<arguments>\n" +
//                "<param name=\"UserId\" value=\"user2\"/>" +
//                "<param name=\"fdd\" value=\"NY\"/>" +
//                "<param name=\"cdrw\" value=\"NY\"/>" +
//                "<param name=\"serial\" value=\"Y\"/>" +
//                "<param name=\"removable\" value=\"NN\"/>\n" +
//                "</arguments>\n" +
//                "</body>\n" +
//                "</request>";
        String source = safePcService.getUserRole(requestApplyPolicyDTO.getUserID());
        UserRole userRole = CommonXMLParser.parsingXML(source);

        Set<String> reqMedias = new LinkedHashSet<>();
        Set<String> reqMeReaddias = new LinkedHashSet<>();
        Set<String> reqMeBlockd = new LinkedHashSet<>();
        for(Param param : userRole.getParam()){
            if(param.getName().equals("UserId")) {
                continue;
            }
            String t = param.getValue();
            String name = param.getName();
            if(isDoubleType(t.length())) {
                if(t.charAt(0) == 'N' && t.charAt(1) == 'N') {
                    reqMeBlockd.add(name);
                    continue;
                }
                if(t.charAt(0) == 'Y') reqMedias.add(name);
                if(t.charAt(1) == 'Y') reqMeReaddias.add(name);
            }else {
                if(t.charAt(0) == 'Y') reqMedias.add(name);
                else if(t.charAt(0) == 'N') reqMeBlockd.add(name);
            }
        }

        if(requestApplyPolicyDTO.getSmartPhoneUseYn() == YesOrNo.YES) {
            reqMeBlockd.remove("smartphone");
            reqMedias.remove("smartphone");
            reqMeReaddias.add("smartphone");
        }
        if(requestApplyPolicyDTO.getWifiUseYn() == YesOrNo.YES) {
            reqMeBlockd.remove("wlan");
            reqMeReaddias.remove("wlan");
            reqMedias.add("wlan");
        }

        if(requestApplyPolicyDTO.getCdUseYn() == RequestType.WRITE ) {
            reqMeBlockd.remove("cdrw");
            reqMeReaddias.add("cdrw");
            reqMedias.add("cdrw");
        } else if (requestApplyPolicyDTO.getCdUseYn() == RequestType.READ) {
            reqMeBlockd.remove("cdrw");
            reqMeReaddias.add("cdrw");
            reqMedias.remove("cdrw");
        }

        if(requestApplyPolicyDTO.getPtbStrgReqType() == RequestType.WRITE) {
            reqMeBlockd.remove("removable");
            reqMeReaddias.add("removable");
            reqMedias.add("removable");
        }else if(requestApplyPolicyDTO.getPtbStrgReqType() == RequestType.READ) {
            reqMeBlockd.remove("removable");
            reqMeReaddias.add("removable");
            reqMedias.remove("removable");
        }

        requestApplyPolicyDTO.setReqMedia(reqMedias.stream().collect(Collectors.toList()));
        requestApplyPolicyDTO.setReqMediaRead(reqMeReaddias.stream().collect(Collectors.toList()));
        requestApplyPolicyDTO.setReqMediaBlock(reqMeBlockd.stream().collect(Collectors.toList()));

        return requestApplyPolicyDTO.toXml();
    }

    public ResultToList postUSBDeviceInfos(PcExecRequestDTO<RequestUSB> requestDTO) {
        ResultToList resultToList = new ResultToList();
        try {
            resultToList.setResult(safePcService.getUsbDeviceInfos(requestDTO.getPcExecRequestDTO().toXml()));
        } catch (IOException e) {
            resultToList.setStatus(CodeConstant.FAIL_CODE);
            resultToList.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
            resultToList.setResult(new ArrayList<>());
        }
        return resultToList;
    }

    public ResultToList postUsbPolicyInfos(PcExecRequestDTO<RequestPolicyUSB> requestDTO) {
        ResultToList resultToList = new ResultToList();
        try {
            resultToList.setResult(safePcService.getUsbPolicyInfos(requestDTO.getPcExecRequestDTO().toXml()));
        } catch (IOException e) {
            resultToList.setStatus(CodeConstant.FAIL_CODE);
            resultToList.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
            resultToList.setResult(new ArrayList<>());
        }
        return resultToList;
    }

    public ResultToObject<ResponseReturnedUsb> postReturnedUsb(PcExecRequestDTO<RequestUSB> requestDTO) {
        ResultToObject<ResponseReturnedUsb> resultToObject = new ResultToObject<>();
        try {
            String source = requestDTO.getPcExecRequestDTO().usbReturnedToXml();
            LOGGER.info("postReturnedUsb xml info {}", source);
//            REAL 코드
            resultToObject.setResult(safePcService.postReturnSecuUsb(source));

            //TODO 테스트용 코드
//            ResponseReturnedUsb responseReturnedUsb = new ResponseReturnedUsb();
//            responseReturnedUsb.setErrorCode("0");
//            responseReturnedUsb.setErrorMsg("OK");
//            resultToObject.setResult(responseReturnedUsb);
//            if(false) throw new IOException();
            //TODO 테스트 코드 엔드
        } catch (IOException e) {
            LOGGER.error("postRetun exception ", e);
            resultToObject.setStatus(CodeConstant.FAIL_CODE);
            resultToObject.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
        }
        return resultToObject;
    }

    public ResultToList releaseNetworkLock(List<PcExecServiceDTO<PcExecNetworkLock>> requestDTO) {
        ResultToList resultToList = new ResultToList();
        for(PcExecServiceDTO<PcExecNetworkLock> pcExecServiceDTO: requestDTO) {
            pcExecServiceDTO.setCode(CodeConstant.SUCCESS_CODE);
            pcExecServiceDTO.setMessage(MsgConstant.SUCCESS_MSG);
            for(PcExecNetworkLock pcExecNetworkLock : pcExecServiceDTO.getDeviceRegists()){
                try {
                    safePcService.releaseNetworkLock(pcExecNetworkLock.getAuditUser(), pcExecNetworkLock.getUserId());
                } catch (Exception e) {
                    pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
                    pcExecServiceDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
                    LOGGER.error("releaseNetworkLock {}, {}, {}", pcExecServiceDTO.getSvcId(), pcExecNetworkLock.getAuditUser(), e.getMessage());
                }
            }
        }
        resultToList.setResult(requestDTO);
        return resultToList;
    }

    private static String encode(byte[] arr){
        Charset utf8charset = Charset.forName("MS949");
        Charset iso88591charset = Charset.forName("UTF-8");

        ByteBuffer inputBuffer = ByteBuffer.wrap( arr );

        // decode UTF-8
        CharBuffer data = utf8charset.decode(inputBuffer);

        // encode ISO-8559-1
        ByteBuffer outputBuffer = iso88591charset.encode(data);
        byte[] outputData = outputBuffer.array();

        return new String(outputData);
    }

    public int fileTake(PcExecTakeOutFileinfoDTO pcExecTakeOutFileinfoDTO) {
        String defaultPath = MarkAnyConfig.ENCFILE_DIR + pcExecTakeOutFileinfoDTO.getDirPath() + File.separator;
        String defaultTargetPath = MarkAnyConfig.ENCFILE_DIR + pcExecTakeOutFileinfoDTO.getDirPath() + File.separator;

        String fileName = pcExecTakeOutFileinfoDTO.getFileName();
        String sourceFullPath = defaultPath + fileName;
        String tempFullPath = defaultPath + pcExecTakeOutFileinfoDTO.getOrgFileName() + pcExecTakeOutFileinfoDTO.getExt();
        String targetFullPath = defaultTargetPath + fileName.substring(0, fileName.lastIndexOf(".")) + ".zip";
        int result = 0;

//        if(new File(targetFullPath).exists()) {
//            LOGGER.info("zip is exists");
//            return result;
//        }
        try {
            File dir = new File(defaultPath);
            if(!dir.exists()) dir.mkdir();
            Files.copy(Paths.get(sourceFullPath), Paths.get(tempFullPath), StandardCopyOption.REPLACE_EXISTING);
            safePcService.takeoutFile(tempFullPath, targetFullPath,
//                            pcExecTakeOutFileinfoDTO.getOrgFileName(), pcExecTakeOutFileinfoDTO.getUserId(), "%ESR_API_PATH%/bin/");
                    pcExecTakeOutFileinfoDTO.getOrgFileName(), pcExecTakeOutFileinfoDTO.getUserId(), "D:/eSpider/bin/");
//                    if(!new File(targetFullPath).exists()) {
//                        throw new IOException("File Not exists..");
//                    }
        } catch (IOException e) {
            LOGGER.error("fileTakeout error " , e.fillInStackTrace());
            result = 1;
        }finally {
            try {
                if(Files.exists(Paths.get(tempFullPath))) Files.delete(Paths.get(tempFullPath));
            } catch (IOException e) {
                LOGGER.error("tempFiel del Error...", e.fillInStackTrace());
            }
        }
        return result;
    }

    /**
     * 보안 USB 파일 반출을 위하여 파일 압축
     *
     * @param requestDTO
     * @return
     */
    public ResultToList fileTakeout(PcExecRequestDTOS<PcExecTakeOutFileinfoDTO> requestDTO) {
        ResultToList resultToLists = new ResultToList();
        List<ResultDRMFileinfoDTO> resultDRMFileinfoDTOS = new ArrayList<>();
        for(PcExecTakeOutFileinfoDTO pcExecTakeOutFileinfoDTO : requestDTO.getPcExecServiceDTOS()){
            int result = 0;
            String code = CodeConstant.SUCCESS_CODE;
            try {
                pcExecTakeOutFileinfoDTO.setOrgFileName(
                        URLDecoder.decode(pcExecTakeOutFileinfoDTO.getOrgFileName(), "utf-8")
                );
                result = this.fileTake(pcExecTakeOutFileinfoDTO);
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("Filename enc error", e.fillInStackTrace());
                result = 1;
            }
            if(result != 0) {
                code = CodeConstant.FAIL_CODE;
            }
            resultDRMFileinfoDTOS.add(new ResultDRMFileinfoDTO(pcExecTakeOutFileinfoDTO.getFileId(), code));
        }
        resultToLists.setResult(resultDRMFileinfoDTOS);

        return resultToLists;
    }


//    private ResultToList commonPostUSBDeviceProposal(List<PcExecServiceDTO<? extends DeviceRegist>> pcExecServiceDTOS, USBMethodType usbMethodType ) {
//        ResultToList resultToList = new ResultToList();
//
//        for(PcExecServiceDTO<? extends DeviceRegist> pcExecServiceDTO : pcExecServiceDTOS) {
//            pcExecServiceDTO.setCode(CodeConstant.SUCCESS_CODE);
//            pcExecServiceDTO.setMessage(MsgConstant.SUCCESS_MSG);
//
//            for(DeviceRegist deviceRegist : pcExecServiceDTO.getDeviceRegists()) {
//                PcExecResult deviceResult;
//                try {
//                    deviceRegist.setAppState("0");
//                    List<PcExecResult> deviceResults = safePcService.postUSBDeviceProposal(deviceRegist.toXml(), usbMethodType);
//                    if(deviceResults.size() == 1) {
//                        deviceResult = deviceResults.get(0);
//                        deviceResult.setCode("0");
//                    }else {
//                        deviceRegist.setAppState("2");
//                        deviceRegist.setReqNo(deviceResults.get(1).getReqNo());
//                        deviceResult  = safePcService.postUSBDeviceProposal(deviceRegist.toXml(), usbMethodType).get(0);
//                    }
//
//                    if(!deviceResult.getCode().equals("0")) {
//                        pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
//                        pcExecServiceDTO.setMessage(deviceResult.getMessage());
//                        LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), deviceResult.getMessage());
//
//                } catch (IOException e) {
//                    pcExecServiceDTO.setCode(CodeConstant.FAIL_CODE);
//                    pcExecServiceDTO.setMessage(MsgConstant.UNKNOWN_EXCEPTION_MSG);
//                    LOGGER.error("USB_DEVICE_FAIL {}, {}, {}", pcExecServiceDTO.getSvcId(), deviceRegist.getDeviceNo(), e.getCause());
//                }
//            }
//        }
//        resultToList.setResult(pcExecServiceDTOS);
//        return resultToList;
//    }

}
